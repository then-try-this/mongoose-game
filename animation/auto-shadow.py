import os

# change canvas size (expand to make space for shadows)
# mogrify -background none -gravity north -extent 365X280 *.png

# adults --------------------------------------------------------- 
# generate shadows
for filename in os.listdir("shadow/adult-frames/"):
    if filename.endswith(".png"):
        os.system("convert shadow/adult-frames/"+filename+" -flip -scale 100%x50% -threshold 99% -channel rgba -blur 0X8 -background transparent -extent 365X280+0-190 shadow/adult-shadows/"+filename)
        # blend out the bottom
        os.system("convert shadow/mult.png shadow/adult-shadows/"+filename+" -alpha on -channel A -compose multiply -composite -channel RGB -negate shadow/adult-shadows/"+filename)

        
# # composite the frames back on the shadows
for filename in os.listdir("shadow/adult-frames/"):
    if filename.endswith(".png"):
        os.system("convert -composite shadow/adult-shadows/"+filename+" shadow/adult-frames/"+filename+" ../client/htdocs/images/sprites/mongoose-adult/left/"+filename)
        
# pups --------------------------------------------------------- 
# generate shadows
for filename in os.listdir("shadow/pup-frames/"):
    if filename.endswith(".png"):
        os.system("convert shadow/pup-frames/"+filename+" -flip -scale 100%x50% -threshold 99% -channel rgba -blur 0X8 -background transparent -extent 200X100+0-40 shadow/pup-shadows/"+filename)
        # blend out the bottom
        os.system("convert shadow/pup-mult.png shadow/pup-shadows/"+filename+" -alpha on -channel A -compose multiply -composite -channel RGB -negate shadow/pup-shadows/"+filename)
        
# composite the frames back on the shadows
for filename in os.listdir("shadow/pup-frames/"):
    if filename.endswith(".png"):
        os.system("convert -composite shadow/pup-shadows/"+filename+" shadow/pup-frames/"+filename+" ../client/htdocs/images/sprites/mongoose-pup/left/"+filename)


