import pygame
import os
pygame.init()

# facts and figures
# -----------------
# orig size: 1762X1029
# working size: 529X309 (resized /3 from source)
# walking crop: 121 143 370 123
# neutral crop: 130 143 370 123
# walk crop: 121 145 370 123
# large crop: (used for other anims): 159 31 365 248
# new large with space for shadows: 365X280


# the process:
# 1. rename file names to remove spaces
# 2. mogrify -resize 33% *.png (on originals)
# 3. crop stationary anims: mogrify -crop 365X280+227+21 *.png
# 4. crop walk and process here
# 5. copy all adult and pup frames to shadow dir
# 6. run shadow process

win = pygame.display.set_mode((529,309), 0, 32)

adult = "2"
prepath = "v3/adult/"+adult+"/"

walk = [pygame.image.load(prepath+'resize/adult-'+adult+'-walk-1.png'),
        pygame.image.load(prepath+'resize/adult-'+adult+'-walk-2.png'),
        pygame.image.load(prepath+'resize/adult-'+adult+'-walk-3.png'),
        pygame.image.load(prepath+'resize/adult-'+adult+'-walk-4.png'),
        pygame.image.load(prepath+'resize/adult-'+adult+'-walk-5.png'),
        pygame.image.load(prepath+'resize/adult-'+adult+'-walk-6.png')]

fns = ['adult-'+adult+'-walk-1.png',
       'adult-'+adult+'-walk-2.png',
       'adult-'+adult+'-walk-3.png',
       'adult-'+adult+'-walk-4.png',
       'adult-'+adult+'-walk-5.png',
       'adult-'+adult+'-walk-6.png']

#1
#crop_x = 121
#crop_y = 31

#2 & 3?
crop_x = 190
crop_y = 21


run = True
frame = 0
c_pos = 0
x_pos = 0
myfont = pygame.font.SysFont('Comic Sans MS', 30)


def draw_transparent_circle(radius, color, transparency=0):
        """transparency is value between 0 and 100, 0 is opaque,
        100 invisible"""
        width, height = radius*2, radius*2
        
        # transparent base surface
        flags = pygame.SRCALPHA
        depth = 32
        base = pygame.Surface((width, height), flags, depth)
        
        # alpha surface
        alpha = int(round(255 * (100-transparency) / 100.0))
        alpha_surface = pygame.Surface((width, height))
        alpha_surface.set_colorkey((0,0,0))
        alpha_surface.set_alpha(alpha)
        
        # draw circle (to alpha surface)
        pygame.draw.circle(alpha_surface, color, (radius,radius), radius)
        
        # draw alpha surface to base surface
        base.blit(alpha_surface, (0,0))
        
        return base

def redrawGameWindow():
    global frame
    global walkCount
    global c_pos
    global x_pos
    global myfont
    
    total_dist = 99
    dist = total_dist/len(walk)
    logical_walk_frame = frame%6
    walk_frame = logical_walk_frame
    if logical_walk_frame==0: x_pos=0
    
    x_pos += [dist*1,
              dist*1,
              dist*1.2,
              dist*0.8,
              dist,
              dist*1.5][walk_frame]

    win.fill((0,255,0))
    print(walk_frame,x_pos-dist)
    win.blit(walk[walk_frame], (x_pos,0))
    textsurface = myfont.render(str(walk_frame+1), False, (255, 255, 255))
    win.blit(textsurface,(0,0))
    
    cmd = "convert \""+prepath+"resize/"+fns[walk_frame]+"\" -crop 365X280+"+str(int(crop_x-(x_pos-dist)))+"+"+str(crop_y)+" +repage \""+prepath+"crop/"+fns[walk_frame]+"\""
    os.system(cmd)
    
    c_pos = dist*walk_frame
    pygame.draw.circle(win,(255,255,255),(int(c_pos)+280,280),5)
    pygame.draw.circle(win,(255,255,255),(int(c_pos)+230,280),5)
    pygame.draw.circle(win,(255,255,255),(int(c_pos)+180,280),5)
    pygame.draw.circle(win,(255,255,255),(int(c_pos)+100,280),5)
    
    if c_pos>500:
        c_pos=0
        
    pygame.display.update()

    img_path = "walk"+str(walk_frame+1)+".png"
    #pygame.image.save(win, img_path)
    #print("image saved to %s" % (img_path))
    
    frame += 1


while run:
    pygame.time.delay(100)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    redrawGameWindow() 
    
    
pygame.quit()
