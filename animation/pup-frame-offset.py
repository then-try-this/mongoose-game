import pygame
import os
pygame.init()

# facts and figures
# -----------------
# orig size: 1762X1029
# working size: 529X309 (resized /3 from source)
# walking crop: 121 143 370 123
# neutral crop: 130 143 370 123
# walk crop: 121 145 370 123
# large crop: (used for other anims): 159 31 365 248
# new large with space for shadows: 365X280

# pup crop: 209 195 200 100

# the process:
# 1. rename file names to remove spaces
# 2. mogrify -resize 33% *.png 
# 3. crop stationary anims
#    mogrify -crop 200X100+180+179 *.png
# 4. crop walk and process here
# 5. run shadow process

win = pygame.display.set_mode((529,309), 0, 32)

pup = "2"
prepath = "v3/pup/"+pup+"/"

# these crops are different x pos than the static ones

#1
#pup_posx = 209
#pup_posy = 195

#2
pup_posx = 170
pup_posy = 179

fns = ["pup-"+pup+"-walk-"+str(i)+".png" for i in range(1,7)]
walk = [pygame.image.load(prepath+"resize/"+fn) for fn in fns]

run = True
frame = 0
c_pos = 0
x_pos = 0
myfont = pygame.font.SysFont('Comic Sans MS', 30)

def redrawGameWindow():
    global frame
    global walkCount
    global c_pos
    global x_pos
    global myfont
    
    total_dist = 34
    dist = total_dist/len(walk)
    logical_walk_frame = frame%6
    walk_frame = logical_walk_frame
    if logical_walk_frame==0: x_pos=0
    
    x_pos += [dist*1,
              dist*1,
              dist*1,
              dist*1,
              dist*1,
              dist*1][walk_frame]

    win.fill((0,255,0))
    print(walk_frame,x_pos-dist)
    win.blit(walk[walk_frame], (x_pos,0))
    textsurface = myfont.render(str(walk_frame+1), False, (255, 255, 255))
    win.blit(textsurface,(0,0))
    
    cmd = "convert \""+prepath+"resize/"+fns[walk_frame]+"\" -crop 200X100+"+str(int(pup_posx-(x_pos-dist)))+"+"+str(pup_posy)+" +repage \""+prepath+"crop/"+fns[walk_frame]+"\""
    os.system(cmd)
    
    c_pos = dist*walk_frame
    pygame.draw.circle(win,(255,255,255),(int(c_pos)+280,280),5)
    pygame.draw.circle(win,(255,255,255),(int(c_pos)+230,280),5)
    pygame.draw.circle(win,(255,255,255),(int(c_pos)+180,280),5)
    pygame.draw.circle(win,(255,255,255),(int(c_pos)+100,280),5)
    
    if c_pos>500:
        c_pos=0
        
    pygame.display.update()

    img_path = "walk"+str(walk_frame+1)+".png"
    #pygame.image.save(win, img_path)
    #print("image saved to %s" % (img_path))
    
    frame += 1


while run:
    pygame.time.delay(800)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    redrawGameWindow() 
    
    
pygame.quit()
