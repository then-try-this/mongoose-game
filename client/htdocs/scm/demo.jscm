;; -*- mode: scheme; -*-
;; Why We Love Wasps Copyright (C) 2019 FoAM Kernow
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define pack-intro-rect (list 0 350 screen-width 210))
(define pack-about-rect (list 1200 480 300 110))

(define (make-demo-mongoose rect)
  (let ((type (choose (list 'male 'female)))	   
		(pos (pack-pos #f rect)))
    (append 
     (make-entity 
      (generate-entity-id!)
      "mongoose" 
      pos 0
      mongoose-adult-sprite-size
      (choose (list "left" "right"))
      (rndf)
      (anim-load-frames
	   (list-ref mongoose-vars (if (eq? type "female") 0 1))
	   mongoose-animation)
      0 mongoose-animation-idle
	  #f 0 pos #f 0 (rndf) (vec2 0 0)
	  mongoose-postrender
	  mongoose-adult-sprite-centre 1.0)
     (list '() 'state-adult #f type #f 'first 100 0 #f 0 0 100))))

(define (make-demo-pup rect)  
  (let ((pos (pack-pos #f rect)))
	(append 
	 (make-entity 
	  (generate-entity-id!)
	  "mongoose" 
	  pos 0
	  mongoose-pup-sprite-size
	  (choose (list "left" "right"))
	  (rndf)
	  (anim-load-frames (choose mongoose-vars) mongoose-animation)
	  0 mongoose-animation-pup #f 0 pos pos 0 (rndf) (vec2 0 0)
	  mongoose-postrender
	  mongoose-pup-sprite-centre 1.0)
	 (list '() 'state-young #f "young" #f 'first 100 0 #f 0 0 100))))

(define (make-new-demo pups adults rect)
  (make-demo
   (append
	(build-list pups (lambda (_) (make-demo-mongoose rect)))
	(build-list adults (lambda (_) (make-demo-pup rect))))
   rect))

(define (make-demo entities rect)
  (list entities rect))

(define (demo-entities d) (list-ref d 0))
(define (demo-rect d) (list-ref d 1))

(define (demo-render d ctx time)
  ;; build and render the entity renderer
  (renderer-render!
   (renderer-add-entities
    (renderer (list))
    (demo-entities d))
   ctx time)

		;; (set! ctx.strokeStyle "#f0f")
		;; (ctx.strokeRect
		;;  (list-ref (demo-rect d) 0)
		;;  (list-ref (demo-rect d) 1)
		;;  (list-ref (demo-rect d) 2)
		;;  (list-ref (demo-rect d) 3))
  )

(define (demo-update d time delta)
  (make-demo
   (map
    (lambda (e)
	  (entity-update (mongoose-demo-update e delta (demo-rect d)) delta))
    (demo-entities d))
   (demo-rect d)))

(define (mongoose-demo-update w delta rect)
  (mongoose-update-animation
   (let ((state (mongoose-state w)))
	 (cond	  
	  ;; young ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	  ((eq? state 'state-young)
	   (mongoose-do-demo-idle w 1 rect))	   
	  
	  ;; adult ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	  ((eq? state 'state-adult)
	   (mongoose-do-demo-idle w 5 rect))

	  ((eq? state 'state-idle-dig-start)
	   (if (> (entity-frame w) 2)
		   (entity-modify-timer
			(mongoose-modify-state w 'state-idle-dig) 0)
		   w))
	  ((eq? state 'state-idle-dig)
	   (if (> (entity-timer w) 3)
		   (mongoose-modify-state w 'state-idle-dig-end)
		   w))
	  ((eq? state 'state-idle-dig-end)
	   (if (> (entity-frame w) 2)
		   (mongoose-do-demo-idle w 1 rect)
		   w))
	  
	  ((eq? state 'state-idle-stand-start)
	   (if (> (entity-timer w) 5)
		   (entity-modify-timer
			(mongoose-modify-state w 'state-idle-stand-end) 0)
		   w))
	  ((eq? state 'state-idle-stand-end)
	   (if (> (entity-frame w) 5)
		   (mongoose-do-demo-idle w 1 rect)
		   w))


	  ((eq? state 'state-idle-lie-start)
	   (if (> (entity-timer w) 5)
		   (entity-modify-timer
			(mongoose-modify-state w 'state-idle-lie-end) 0)
		   w))
	  ((eq? state 'state-idle-lie-end)
	   (if (> (entity-frame w) 2)
		   (mongoose-do-demo-idle w 1 rect)
		   w))

	  ((eq? state 'state-idle-sit-start)
	   (if (> (entity-timer w)
			  (if (eq? (mongoose-type w) 'young)
				  1 5))
		   (entity-modify-timer
			(mongoose-modify-state w 'state-idle-sit-end) 0)
		   w))
	  ((eq? state 'state-idle-sit-end)
	   (if (> (entity-frame w) 2)
		   (mongoose-do-demo-idle w 1 rect)
		   w))

	  ;; common ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	  ((eq? state 'state-wander)
	   (if (>= (entity-walk-time w) 1)		   
		   (entity-modify-timer (mongoose-modify-state w 'state-end-wander) 0)
		   (entity-walk
			w (if (or (eq? (mongoose-type w) 'pup)
					  (eq? (mongoose-type w) 'young))
				  mongoose-pup-speed
				  mongoose-speed) delta)))
	  
	  ((eq? state 'state-end-wander)	   
	   (if (>= (entity-timer w) 1)		   
		   (entity-modify-timer
			(mongoose-modify-state
			 w (cond
				((eq? (mongoose-type w) 'pup) 'state-pup)
				((eq? (mongoose-type w) 'young) 'state-young)
				(else 'state-adult)))
			0)
		   w))


	  (else w)))
   delta))
  
(define (mongoose-demo-start-wandering w rect)
  (mongoose-modify-state
   (mongoose-walk-to-pos w (pack-pos w rect))
   'state-wander))

(define (mongoose-do-demo-idle w freq rect)
  (if (> (entity-timer w) freq)
	  (if (< (rndf) 0.75)
		  (entity-modify-timer
		   (if (< (rndf) 0.5)
			   (mongoose-demo-start-wandering w rect)
			   (mongoose-modify-state
				w
				(if (or (eq? (mongoose-type w) 'pup)
						(eq? (mongoose-type w) 'young))					
					;; pups only have sit idle behaviour
					'state-idle-sit-start
					(choose
					 (if (mongoose-escort w)
						 ;; standing with escort glow looks silly
						 (list 'state-idle-dig-start
							   'state-idle-sit-start
							   'state-idle-lie-start)	
						 (list 'state-idle-dig-start
							   'state-idle-stand-start
							   'state-idle-sit-start
							   'state-idle-lie-start)))))) 0)
		  (entity-modify-timer w 0))
	  w))

