;; -*- mode: scheme; -*-
;; Why We Love Wasps Copyright (C) 2019 FoAM Kernow
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define (make-event type time-left data fn cond-fn prey) 
  (list type time-left data fn cond-fn prey))

(define (event-type e) (list-ref e 0))
(define (event-time-left e) (list-ref e 1))
(define (event-modify-time-left e s) (list-replace e 1 s))
(define (event-data e) (list-ref e 2))
(define (event-modify-data e s) (list-replace e 2 s))
(define (event-fn e) (list-ref e 3))
(define (event-cond-fn e) (list-ref e 4))
(define (event-prey e) (list-ref e 5))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (event-update event pack delta)
  (event-modify-time-left
   (cond
    ((eq? (event-type event) 'dung) (event-dung-update event pack delta))
    ((eq? (event-type event) 'rubbish) (event-rubbish-update event pack delta))
    ((eq? (event-type event) 'warthog) (event-warthog-update event pack delta))
    ((eq? (event-type event) 'nest) (event-nest-update event pack delta))
    ((eq? (event-type event) 'normal) (event-normal-update event pack delta))
    ((eq? (event-type event) 'stork) (event-stork-update event pack delta))
    ((eq? (event-type event) 'snake) (event-snake-update event pack delta))
    ((eq? (event-type event) 'snake-forage) (event-snake-forage-update event pack delta))
    ((eq? (event-type event) 'drought) (event-drought-update event pack delta))
	
    (else event))
   (- (event-time-left event) delta)))

(define (event-render ctx event)
    ;; event specific background animation
    (cond
     ((eq? (event-type event) 'dung) (event-dung-render ctx event))
     ((eq? (event-type event) 'rubbish) (event-rubbish-render ctx event))
     ((eq? (event-type event) 'warthog) (event-warthog-render ctx event))
     ((eq? (event-type event) 'nest) (event-nest-render ctx event))
     ((eq? (event-type event) 'normal) (event-normal-render ctx event))
     ((eq? (event-type event) 'stork) (event-stork-render ctx event))
     ((eq? (event-type event) 'snake) (event-snake-render ctx event))
     ((eq? (event-type event) 'snake-forage) (event-snake-forage-render ctx event))
     ((eq? (event-type event) 'drought) (event-drought-render ctx event))
     (else 0)))

(define (pick-random-forage-event location weather)
  (if (eq? weather "drought")
	  (event-drought-builder)
	  (if (eq? location "wild")
		  (choose (list
				   (event-dung-builder)
				   (event-nest-builder)
				   (event-normal-builder)
				   (event-normal-builder)
				   (event-normal-builder)
				   (event-snake-forage-builder)
				   ))
		  (choose (list
				   (event-rubbish-builder)
				   (event-warthog-builder)
				   (event-normal-builder)
				   (event-normal-builder)
				   (event-normal-builder)
				   (event-snake-forage-builder)
				   )))))

(define (pick-random-good-forage-event location)
  (if (eq? location "wild")
	  (choose (list
			   (event-dung-builder)
			   (event-nest-builder)
			   (event-normal-builder)
			   (event-normal-builder)
			   (event-normal-builder)			   
			   ))
	  (choose (list
			   (event-rubbish-builder)
			   (event-warthog-builder)
			   (event-normal-builder)
			   (event-normal-builder)
			   (event-normal-builder)
			   ))))

(define (pick-random-event location)
  (if (eq? location "wild")
	  (choose (list
			   (event-snake-builder)
			   ))
	  (choose (list
			   (event-snake-builder)
			   (event-stork-builder)
			   ))))


