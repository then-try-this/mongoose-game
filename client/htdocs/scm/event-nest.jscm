;; -*- mode: scheme; -*-
;; Why We Love Wasps Copyright (C) 2019 FoAM Kernow
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define nest-min-xpos 1050)
(define nest-start-xpos 1650)
(define nest-ypos 600)

(define (event-data-nest xpos frame state) (list xpos frame state))
(define (event-data-nest-xpos e) (list-ref (event-data e) 0))
(define (event-data-nest-frame e) (list-ref (event-data e) 1))
(define (event-data-nest-state e) (list-ref (event-data e) 2))

(define (event-nest-builder)
  (lambda ()
    (make-event 
     'nest 99999999999
     (event-data-nest nest-start-xpos 0 'none)
     (lambda (event pack delta state)
       (cond
		((eq? state 'first) pack)
		((eq? state 'last) pack)
		(else pack)))
     (lambda (pack)
       #t)
	 'egg)))

(define (event-nest-update event pack delta)
  (let ((event-state (window-event-state (pack-window pack))))
	(event-modify-data
	 event
	 (event-data-nest
	  (cond
	   ((eq? event-state 'appear) (max nest-min-xpos (- (event-data-nest-xpos event) (* 220 delta))))
	   ((eq? event-state 'stop) (event-data-nest-xpos event))
	   ((eq? event-state 'dissapear) (min nest-start-xpos (+ (event-data-nest-xpos event) (* 220 delta))))
	   (else nest-start-xpos))
	  (+ (event-data-nest-frame event) (* 2 delta))
	  event-state))))


(define (event-nest-render ctx event)
  (ctx.drawImage
   (find-image
	(string-append "events/event-nest.png"))
   (floor (event-data-nest-xpos event))
   (floor nest-ypos)))


