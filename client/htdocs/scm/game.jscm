;; -*- mode: scheme; -*-
;; Copyright (C) 2021 Then Try This
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(load "scm/maths.jscm")
(load "scm/bezier.jscm")
(load "scm/random.jscm")
(load "scm/nightjar.jscm")
(load "scm/sound.jscm")
(load "scm/local-storage.jscm")

(define params-version 10)
(define player-id -1)
(define player-name "???")
(define score-per-generation 100)
(define score-per-grownup 10)
(define end-game-time 7)
(define installation-mode #f)
										;(define installation-mode #t)

(let ((v (local-load "mongoose-game-version")))
  (when (or (not v) (< v params-version))
		(local-nuke)
		(local-save "mongoose-game-version" params-version)
		))

(define (local-get-number name)
  (string->number (local-get-param "mongoose-game" name)))

(local-setup-params 
 "mongoose-game" 
 (list
  (make-param "mongoose-debug" 0 "1 or 0")
  (make-param "mongoose-start-energy" 7.5 "energy units")
  (make-param "mongoose-speed" 300 "pixels per second")
  (make-param "mongoose-pup-speed" 350 "pixels per second")
  (make-param "mongoose-forage-speed" 400 "pixels per second")
  (make-param "mongoose-unescorted-forage-speed" 200 "pixels per second")
  (make-param "mongoose-energy-drain" 0.01 "energy units per sec")
  (make-param "mongoose-hungry-threshold" 2 "energy units")
  (make-param "mongoose-max-energy" 10 "energy units")
  (make-param "mongoose-build-time" 1 "seconds")
  (make-param "mongoose-giving-birth-time" 2.5 "seconds")
  (make-param "mongoose-tending-time" 1 "seconds")
  (make-param "mongoose-forage-time" 2 "seconds")
  (make-param "mongoose-dying-time" 3 "seconds")
  (make-param "mongoose-birth-time" 10 "seconds")
  (make-param "mongoose-birth-wait-time" 10 "seconds")
  (make-param "mongoose-reproductive-threshold" 0 "energy")
  (make-param "mongoose-leave-den-time" 1 "months")
  (make-param "mongoose-adult-time" 6 "months")
  (make-param "pack-year-length" 300 "seconds")
  (make-param "pack-num-years" 2 "years")
  (make-param "mongoose-forage-cost" 0 "energy units")
  (make-param "mongoose-new-pup-cost" -2 "energy units")
  (make-param "mongoose-babysit-cost" -1 "energy units")
  (make-param "mongoose-escort-cost" -1 "energy units")
  (make-param "pack-start-mongooses" 2 "mongooses")
  (make-param "event-start-time" 75 "seconds") ;; 3 months
  (make-param "event-time-between-events" 20 "seconds")
  (make-param "mongoose-egg-benefit" 5 "energy units")
  (make-param "mongoose-lizard-benefit" 3 "energy units")
  (make-param "mongoose-beetle-benefit" 1 "energy units")
  (make-param "mongoose-caterpillar-benefit" 1 "energy units")
  (make-param "mongoose-snail-benefit" 1 "energy units")
  (make-param "mongoose-fruit-benefit" 0.5 "energy units")
  (make-param "mongoose-dung-beetle-benefit" 3 "energy units")
  (make-param "mongoose-rubbish-benefit" 5 "energy units")
  (make-param "mongoose-tick-benefit" 5 "energy units")
  ))

(load "scm/ordered-list.jscm")
(load "scm/translations.jscm")
(load "scm/i18n.jscm")
(load "scm/particles.jscm")
(load "scm/animation.jscm")
(load "scm/entity.jscm")
(load "scm/entity-list.jscm")
(load "scm/entity-renderer.jscm")
(load "scm/window.jscm")
(load "scm/mongoose.jscm")
(load "scm/event.jscm")
(load "scm/event-normal.jscm")
(load "scm/event-dung.jscm")
(load "scm/event-rubbish.jscm")
(load "scm/event-warthog.jscm")
(load "scm/event-nest.jscm")
(load "scm/event-stork.jscm")
(load "scm/event-snake.jscm")
(load "scm/event-snake-forage.jscm")
(load "scm/event-drought.jscm")
(load "scm/scores.jscm")
(load "scm/time.jscm")
(load "scm/pack.jscm")
(load "scm/menu.jscm")
(load "scm/info.jscm")
(load "scm/demo.jscm")
(load "scm/score-popup.jscm")

(define (button-sound)
  (play-sound "button.wav"))

(define default-button-x (- (/ screen-width 2) 150))
(define default-button-y (+ (/ screen-height 2) 200))
(define default-button-width 300)
(define default-button-height 80)

(define button-gap 250)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (empty-nightjar-data player-id location time) (make-new-pack player-id location time))

(define (register-game c player-id location)
  (server-call-mutate
   "game"
   (list (list "player_id" player-id)
		 (list "location" location)) 
   (lambda (c game-id)	 
     (reset-scores!)
     (nightjar-game-screen c game-id location)))
  c)

(define (send-update-scores c)
  (server-call-mutate
   "score"
   (list
    (list "game_id" (pack-game-id (game-data c)))
    (list "births" (scores-births))
    (list "deaths" (scores-deaths))
    (list "max_pop" (scores-max-pop))
    (list "generations" (scores-generations))
    (list "foraged" (scores-foraged))
	(list "grown_ups" (scores-grown-ups))
	(list "lifeforce" (scores-final-lifeforce))	
    (list "score" (+ (* score-per-grownup (scores-grown-ups))
					 (scores-final-lifeforce)
					 (* score-per-generation (scores-generations))))
	)
   (lambda (c score-pos)
     (nightjar-enter-name c (JSON.parse score-pos))
	 ))
  c)

(define (register c location)
  ;; fixed one player per game...
  (set! player-name "???")
  (server-call-mutate
   "player"
   '() 
   (lambda (c id)	 
     (set! player-id id)
     (register-game c player-id location)))
  c)

(define (nightjar-intro c)
  (game-modify-timeout
   (lambda ()
	 (set! window.location "/"))
   (game-modify-data
	(lambda (d)
	  (make-new-demo 3 5 pack-intro-rect))
	(game-modify-update
	 (lambda (t c)
	   (game-modify-data
		(lambda (d)
		  (demo-update (game-data c) (/ t 1000) (game-delta c)))
		c))
	 (game-modify-render
	  (lambda (ctx)
		(demo-render (game-data c) ctx (game-time c))
		(set! ctx.fillStyle text-col)
		(set! ctx.font "normal 80pt arvo")
		(wrap-text ctx (trans 'title) 0 120 1500 100)	  
		(set! ctx.font "normal 30pt arvo")
		(wrap-text ctx (trans 'main-strap) 0 200 1000 50)
		(set! ctx.font "normal 25pt arvo")

		(ctx.drawImage (find-image "windows/tree 1 lines.png") 1300 370)
		(ctx.drawImage (find-image "windows/tree 3 lines.png") 0 290)
		(ctx.drawImage (find-image "windows/tree 2 lines.png") -20 380)

		(ctx.drawImage (find-image "logos.png") 500 620)
		(set! ctx.font "normal 12pt arvo")
		(ctx.fillText "version 1.2" 1500 745)
		
		)
	  
	  (game-modify-buttons
	   (list
		
		(rounded-button
		 (trans 'play-button) 
		 (- default-button-x 350)
		 (- default-button-y 30)
		 default-button-width default-button-height
		 "none"
		 (lambda (c)
		   (button-sound)
		   (nightjar-location c)
		   ;;(nightjar-enter-name c 1)
		   ;;(nightjar-end c 99)
		   ))
		
		(rounded-button
		 (trans 'scores-button)
		 default-button-x
		 (- default-button-y 30)
		 default-button-width default-button-height
		 "none"
		 (lambda (c)
		   (button-sound)
		   (server-call-mutate
		    "hiscores"
		    (list)
		    (lambda (c scores)	      
			(nightjar-hiscores c (JSON.parse scores))))
		   c))
		
		
		(rounded-button
		 (trans 'about-button)
		 (+ default-button-x 350)
		 (- default-button-y 30)
		 default-button-width default-button-height
		 "none"
		 (lambda (c)
		   (button-sound)
		   (nightjar-about c)))
		)
	   c))))))

(define (nightjar-location c)
  (game-modify-render
   (lambda (ctx)
     ;;(demo-render (game-data c) ctx)
	 (set! ctx.fillStyle text-col)
     (set! ctx.font "normal 75pt arvo")
     (wrap-text ctx (trans 'location-title) 0 150 1500 100)    
     (set! ctx.font "normal 30pt arvo")
     (wrap-text ctx (trans 'location-text) 0 250 1300 50)    
     (set! ctx.font "normal 50pt arvo")
     )
   
   (game-modify-buttons
    (list
     
     (image-button
      (trans 'location-village)
      (+ (/ screen-width 2) 200) 
      (- default-button-y 50)
      "none"
      (find-image "village-button.png")
      (lambda (c)
		(button-sound)
		(register c "village")
		c))

     (image-button
      (trans 'location-wild)
      (- (/ screen-width 2) 200) 
      (- default-button-y 50)
      "none"
      (find-image "wild-button.png") 
      (lambda (c)
		(button-sound)
		(register c "wild")
		c))

	 (rounded-button
	  (trans 'back-button) 
	  100
	  (+ default-button-y 50)
	  default-button-width default-button-height
	  "none"
	  (lambda (c)
		(button-sound)
		(nightjar-intro c)
		))

	 
     )
    c)))

(define (nightjar-about c)
  (game-modify-data
   (lambda (d)
	 (make-new-demo 1 2 pack-about-rect))
   (game-modify-update
	(lambda (t c)
	  (game-modify-data
	   (lambda (d)
		 (demo-update (game-data c) (/ t 1000) (game-delta c)))
	   c))
	(game-modify-render
	 (lambda (ctx)
	   (ctx.drawImage (find-image "pages/about.png") 50 10)	   
	   (demo-render (game-data c) ctx (game-time c))
	   (set! ctx.font "normal 25pt arvo"))
	 
	 (game-modify-buttons
	  (list
	   (rounded-button
		"BACK"
		(- default-button-x 300)
		(+ default-button-y 50)
		default-button-width 
		default-button-height 
		"none"
		(lambda (c)
		  (button-sound)
		  (nightjar-intro c)))

	   (rounded-button
		"MORE ABOUT MONGOOSES"
		(+ default-button-x 100)
		(+ default-button-y 50)
		(+ default-button-width 210) 
		default-button-height 
		"none"
		(lambda (c)
		  (button-sound)
		  (nightjar-about2 c))))
	  c)))))

(define (nightjar-about2 c)
  (game-modify-render
   (lambda (ctx)
	 (set! ctx.font "normal 25pt arvo")
	 (ctx.drawImage (find-image "pages/about2.png") 50 50)	   
	 (set! ctx.font "normal 25pt arvo")

	 (index-for-each
	  (lambda (i pos)
		(let ((frame (floor (+ (/ (game-time c) (* 300 (list-ref (list 1 0.94 1.05) i))) i))))
		  (let ((dir (list-ref (list "left" "right" "left") i)))
			(ctx.drawImage
			 (find-image
			  (string-append "events/beetle-" dir "/beetle-" (+ 1 (pingpong frame 3)) ".png")) (vx pos) (vy pos))		 
			)))
	  (list
	   (vec2 1080 620)
	   (vec2 1202 630)
	   (vec2 1000 654)
	   )))

   (game-modify-buttons
	(append
	 (list
	  (rounded-button
	   "BACK"
	   default-button-x
	   (+ default-button-y 50)
	   default-button-width 
	   default-button-height 
	   "none"
	   (lambda (c)
		 (button-sound)
		 (nightjar-intro c)))))
	c)))


(define (nightjar-game-screen c player-id location)
  (game-modify-data
   (lambda (d)
     (empty-nightjar-data player-id location (/ (game-time c) 1000)))
   (game-modify-mouse-hook
    (lambda (state c)
      (game-modify-data
       (lambda (d)
		 (pack-update-mouse
		  (game-data c) state (game-mx c) (game-my c)))
       c))   
    (game-modify-render
     (lambda (ctx)
       (pack-render (game-data c) ctx (game-time c)))
     (game-modify-update
      (lambda (t c)
		(game-modify-data
		 (lambda (d)
		   (pack-update (game-data c) (/ t 1000) (game-delta c)))
		 (if (> (pack-end-game-timer (game-data c)) end-game-time)			 
			 (game-modify-update ;; turn off update so we only send once
			  (lambda (t c) c)		   
			  (send-update-scores c))
			 c)))
      (game-modify-buttons
       (list
		(rounded-button
		 (trans 'game-quit)
		 (- screen-width 105) 5 100 60 #f
		 (lambda (c)
		   (button-sound)
		   (sound-layers-stop! (pack-sound-layers (game-data c)))
		   ;; (update-scores-final-lifeforce!
		   ;; 	(pack-calc-total-lifeforce (game-data c)))
		   ;; (clear-scores-analysis!)
		   ;; (send-update-scores c)
		   (nightjar-intro game)
		   ))
		) c))))))

(define scores-anim-t 0)
(define scores-anim-genreations 0)
(define scores-anim-grown-ups 0)
(define scores-anim-lifeforce 0)
(define scores-anim-timing 0.10) 

(define (nightjar-end c rank)
  (set! scores-anim-pos 0)
  (set! scores-anim-generations -1)
  (set! scores-anim-grown-ups -1)
  (set! scores-anim-lifeforce -1)
  (game-modify-mouse-hook
   (lambda (state c) c)
   (game-modify-render
    (lambda (ctx)
      (set! ctx.fillStyle text-col)      
      (set! ctx.font "normal 30pt arvo")
      
      (let ((n (game-data c)))		
		(set! ctx.fillStyle highlight2-col)
		(ctx.fillRect (- (/ screen-width 2) 330) 50 660 280)
		(set! ctx.fillStyle text-col)      
		
		(ctx.fillText (string-append "Grown up pups: " scores-anim-grown-ups) 500 100)
		(set! ctx.font "bold 30pt arvo")	
		(when (>= scores-anim-grown-ups 0)
			  (ctx.fillText (string-append "" (* scores-anim-grown-ups score-per-grownup) " points") 910 100))
		(set! ctx.font "normal 30pt arvo")

		(ctx.fillText (string-append "Final life force: " (max 0 scores-anim-lifeforce)) 500 200)
		(set! ctx.font "bold 30pt arvo")	
		(when (>= scores-anim-lifeforce 0)
			  (ctx.fillText (string-append "" scores-anim-lifeforce " points") 910 200))
		(set! ctx.font "normal 30pt arvo")
		
		(ctx.fillText (string-append "Generations: " (max scores-anim-generations 0)) 500 300)
		(set! ctx.font "bold 30pt arvo")	
		(when (>= scores-anim-generations 0)
			  (ctx.fillText (string-append "" (* scores-anim-generations score-per-generation) " points") 910 300))
		(set! ctx.font "normal 30pt arvo")
		
		(ctx.fillText "YOUR TOTAL SCORE:" 450 400)
		(set! ctx.font "bold 30pt arvo")	
		(when (>= scores-anim-generations 0)
			  (ctx.fillText (string-append "" (floor (+ (* score-per-grownup scores-anim-grown-ups)
														scores-anim-lifeforce
														(* score-per-generation scores-anim-generations))) " points") 960 400))
		(set! ctx.font "normal 30pt arvo")
		
		(if (< rank 9)
			(fill-centre-text ctx (string-append "You are position " (+ rank 1) " on the leaderboard!") (/ screen-width 2) 470)
			(fill-centre-text ctx (string-append "You are position " (+ rank 1) " - try again for a top ten leaderboard position...")  (/ screen-width 2) 470))

		(when (eq? scores-anim-generations (scores-generations))	      
			  (wrap-text ctx (scores-analysis) 0 530 1300 50))

		(ctx.drawImage (find-image "sprites/mongoose-adult/right/adult-3-stand-4.png") 100 100)
		(ctx.drawImage (find-image "sprites/mongoose-adult/left/adult-2-stand-4.png") 1150 100)
		
		(set! ctx.font "normal 25pt arvo")
		)
      
      ;; (let ((frame (+ (modulo (floor (/ (game-time c) 300)) 2) 1)))
	  ;; 	(ctx.drawImage (find-image (string-append "sprites/wasp/right/2-idle-" frame ".png")) 100 100)
	  ;; 	(ctx.drawImage (find-image (string-append "sprites/wasp/left/5-idle-" frame ".png")) 1120 100))
      
      ;; (ctx.drawImage (find-image "title-wildflowers1.png") -50 290)
      ;; (ctx.drawImage (find-image "title-wildflowers2.png") 1280 340)

      )
    (game-modify-update
     (lambda (t c)
       (when (and (> scores-anim-t scores-anim-timing)
				  (< scores-anim-generations (scores-generations)))
			 (set! scores-anim-t 0)
			 (cond
			  ((< scores-anim-grown-ups (scores-grown-ups))
			   (play-sound "power-up.wav")
			   (set! scores-anim-grown-ups (+ scores-anim-grown-ups 1)))
			  ((< scores-anim-lifeforce (floor (scores-final-lifeforce)))
			   (play-sound "power-up.wav")
			   (set! scores-anim-lifeforce (+ scores-anim-lifeforce 100))
			   (when (>= scores-anim-lifeforce (floor (scores-final-lifeforce)))
					 (set! scores-anim-lifeforce (floor (scores-final-lifeforce)))))
			  (else
			   (play-sound "power-up-2.wav")
			   (set! scores-anim-generations (+ scores-anim-generations 1)))))
       (set! scores-anim-t (+ scores-anim-t (game-delta c)))
       c)
     (game-modify-buttons
      (list
	   
       (rounded-button
		"PLAY AGAIN"
		(- default-button-x 350)
		(+ default-button-y 50)
		default-button-width default-button-height
		"none"
		(lambda (c)
		  (button-sound)
		  (nightjar-location c)
		  ))
       
       (rounded-button
		"LEADERBOARD"
		default-button-x
		(+ default-button-y 50)
		default-button-width default-button-height
		"none"
		(lambda (c)
		  (button-sound)
		  (server-call-mutate
		   "hiscores"
		   (list)
		   (lambda (c scores)	      
			 ;; restart the demo!
			 (nightjar-hiscores
			  (game-modify-update
			   (lambda (t c)
				 ;; update loop
				 (game-modify-data
				  (lambda (d)
					d ;;(demo-update (game-data c) (/ t 1000) (game-delta c))
					)
				  c))
			   ;; init the demo
			   (game-modify-data
				(lambda (d)
				  d ;;(make-new-demo)
				  )
				c))
			  (JSON.parse scores))))
		  c))
       
       (rounded-button
		"QUIT"
		(+ default-button-x 350)
		(+ default-button-y 50)
		default-button-width 
		default-button-height 
		"none"
		(lambda (c)
		  (button-sound)
		  (nightjar-intro game))))
      c)))))

(define col1 0)
(define col2 130)
(define col3 295)
(define col4 440)
(define col5 580)

(define (render-leaderboard c x y scores)

  (set! ctx.font "normal 16pt arvo")
  (wrap-text ctx "NAME" (+ x col1) 170 1000 1)
  (wrap-text ctx "GENERATIONS" (+ x col2) 170 1000 1)
  (wrap-text ctx "GROWN UPS" (+ x col3) 170 1000 1)
  (wrap-text ctx "LIFE FORCE" (+ x col4) 170 1000 1)
  (wrap-text ctx "SCORE" (+ x col5) 170 1000 1)
  
  (when scores
		(index-for-each
		 (lambda (i e)
		   (let ((space 40))
		   (cond
			((zero? i)
			 (set! ctx.font "bold 20pt arvo")
			 (wrap-text ctx (list-ref e 0) (+ x col1) (+ 220 (* space i)) 1000 1)
			 (wrap-text ctx (+ "" (list-ref e 1)) (+ x col2) (+ 220 (* space i)) 1000 1)
			 (wrap-text ctx (+ "" (list-ref e 2)) (+ x col3) (+ 220 (* space i)) 1000 1)
			 (wrap-text ctx (+ "" (floor (list-ref e 3))) (+ x col4) (+ 220 (* space i)) 1000 1)
			 (wrap-text ctx (+ "" (floor (list-ref e 4)) " points") (+ x col5) (+ 220 (* space i)) 1000 1)
			 (set! ctx.font "normal 20pt arvo"))
			(else
			 (wrap-text ctx (list-ref e 0) (+ x col1) (+ 220 (* space i)) 1000 1)
			 (wrap-text ctx (+ "" (list-ref e 1)) (+ x col2) (+ 220 (* space i)) 1000 1)
			 (wrap-text ctx (+ "" (list-ref e 2)) (+ x col3) (+ 220 (* space i)) 1000 1)
			 (wrap-text ctx (+ "" (floor (list-ref e 3))) (+ x col4) (+ 220 (* space i)) 1000 1)
			 (wrap-text ctx (+ "" (floor (list-ref e 4)) " points") (+ x col5) (+ 220 (* space i)) 1000 1)))))
		 scores)))


(define (nightjar-hiscores c scores)
  (game-modify-render
   (lambda (ctx)
     ;;(demo-render (game-data c) ctx)
     (set! ctx.fillStyle text-col)      
     (set! ctx.font "normal 50pt arvo")
     (wrap-text ctx "Wild pack scores" -380 100 1300 100)
     (wrap-text ctx "Village pack scores" 400 100 1300 100)
     (set! ctx.font "normal 20pt arvo")
     
	 (render-leaderboard c -670 0 (list-ref scores 0))
	 (render-leaderboard c 100 0 (list-ref scores 1))

	 (set! ctx.font "normal 25pt arvo")

	 (ctx.drawImage
	  (find-image
	   (string-append "events/event-warthog-" (+ 1 (modulo (floor (/ (game-time c) 100)) 3)) ".png")) 1280 590)
     
     )
   (game-modify-buttons
    (list
     (rounded-button
      "BACK"
	  default-button-x
	  (+ default-button-y 50)
	  default-button-width 
	  default-button-height 
	  "none"
	  (lambda (c)
		(button-sound)
		(nightjar-intro game))))
    c)))

(define (type-into str ch)
  (car
   (foldl
    (lambda (c r)
      (if (and (not (cadr r)) (eq? c "?"))
          (list (string-append (car r) ch) #t)
          (list (string-append (car r) c) (cadr r))))
    (list "" #f)
    (str.split ""))))

(define (type-into-delete str)
  (car (foldl
        (lambda (c r)
          ;;(console.log (list c r))
          (if (and (not (cadr r)) (not (eq? c "?")))
              (list (string-append "?" (car r)) #t)
              (list (string-append c (car r)) (cadr r))))
        (list "" #f)
        (reverse (str.split "")))))

(define (type-username ch c)
  (set! player-name (type-into player-name ch)))

(define (type-delete c)
  (set! player-name (type-into-delete player-name)))


(define (qwertypos-x i)  
  (cond ((< i 10) i)
		((and (> i 9) (< i 19)) (- i 10))
		(else (- i 19))))

(define (qwertypos-y i)  
  (cond ((< i 10) 0)
		((and (> i 9) (< i 19)) 1)
		(else 2)))

(define (nightjar-enter-name c rank)
  (game-modify-mouse-hook
   (lambda (state c) c)
   (game-modify-update
    (lambda (t c) c)
    (game-modify-render
     (lambda (ctx)
       (set! ctx.font "normal 30pt arvo")
       (wrap-text ctx "Enter your name for the leaderboard" 0 70 1000 70)
       (set! ctx.font "normal 50pt arvo")
       (wrap-text ctx player-name 0 150 1000 70)
       (set! ctx.font "normal 25pt arvo"))

     (game-modify-buttons
      (append
       (index-map
		(lambda (i ch)
		  (let ((x (+ (cond ((eq? (qwertypos-y i) 0) 250)
							((eq? (qwertypos-y i) 1) 300)
							(else 380))
					  (* (qwertypos-x i) 120)))
				(y (+ 220 (* (qwertypos-y i) 130)))
				(img (find-image "paw.png")))
			(image-button ch x y #f
						  img
						  (lambda (c)
							(button-sound)
							(type-username ch c)
							c))))
		(list "Q" "W" "E" "R" "T" "Y" "U" "I" "O" "P"
			  "A" "S" "D" "F" "G" "H" "J" "K" "L"
			  "Z" "X" "C" "V" "B" "N" "M"))

       (list
		(rounded-button "DELETE" (- default-button-x 200) default-button-y default-button-width default-button-height "none"
					 (lambda (c)
					   (button-sound)		      
					   (type-delete c)
					   c))

		(rounded-button "DONE"  (+ default-button-x 200) default-button-y default-button-width default-button-height "none"
					 (lambda (c)
					   (button-sound)
					   (server-call
						"player-name"
						(list
						 (list "player_id" player-id)
						 (list "player_name" player-name)))		       
					   (button-sound)		       
					   (nightjar-end game rank)))
		))
      c)))))


(set! ctx.font "normal 75pt arvo")
(set! ctx.fillStyle "#606353")

(load-sounds!
 (list
  ;; game sounds
  (list "button.wav" 8)
  (list "score-up.wav" 8)
  (list "score-up-mate.wav" 8)
  (list "notify.wav" 8)
  (list "action-1.wav" 8)
  (list "action-2.wav" 8)
  (list "drought.wav" 8)
  (list "drought-2.wav" 8)
  (list "drought-3.wav" 8)
  (list "food.wav" 8)
  (list "notify.wav" 8)
  (list "power-up.wav" 8)
  (list "power-up-2.wav" 8)
  (list "power-up-3.wav" 8)
  (list "snake.wav" 8)
  (list "stork.wav" 8)
  (list "warning.wav" 8)
  (list "no-energy-click.wav" 8)

  ;; mongoose sounds
  (list "chatter-1.wav" 2)
  (list "chatter-2.wav" 2)
  (list "chatter-3.wav" 2)
  (list "chatter-4.wav" 2)
  (list "chatter-5.wav" 2)
  (list "chatter-6.wav" 2)
  (list "screech-1.wav" 2)
  (list "screech-2.wav" 2)
  (list "screech-3.wav" 2)
  (list "screech-4.wav" 2)
  (list "squeak-1.wav" 2)
  (list "squeak-2.wav" 2)
  (list "squeak-3.wav" 2)
  
  ;; background sounds
  (list "birdloop-1.wav" 1)
  (list "birdloop-2.wav" 1)
  (list "birdloop-3.wav" 1)
  (list "rain.wav" 1)

  ))

(load-images!
 (append 
  (anim->filenames mongoose-animation)
  (list 
   "paw.png"
   "sprites/rain.png"
   "sprites/rain2.png"
   "sprites/menu-triangle-repro.png"
   "backgrounds/village-back.png"
   "backgrounds/village-back-drought.png"
   "backgrounds/village-front.png"
   "backgrounds/village-front-drought.png"
   "backgrounds/wild-back.png"
   "backgrounds/wild-back-drought.png"
   "backgrounds/wild-front.png"
   "backgrounds/wild-front-drought.png"
   "backgrounds/horizon-grass.png"
   "backgrounds/horizon-mud.png"
   "backgrounds/drought.png"
   "windows/grass leaves 1.png"
   "windows/grass leaves 2.png"
   "windows/grass leaves 3.png"
   "windows/tree 1 lines.png"
   "windows/tree 2 lines.png"
   "windows/tree 3 lines.png"
   "windows/mango tree.png"
   "windows/bench.png"
   "windows/bin bag.png"
   "windows/bike.png"
   "windows/tyre 1.png"
   "logos.png"
   "village-button.png"
   "wild-button.png"
   "events/event-dung-1.png"
   "events/event-dung-2.png"
   "events/event-rubbish.png"
   "events/event-warthog-1.png"
   "events/event-warthog-2.png"
   "events/event-warthog-3.png"
   "events/event-nest.png"
   "events/event-normal.png"
   "events/event-stork-1.png"
   "events/event-stork-2.png"
   "events/event-stork-popup.png"
   "events/event-snake.png"
   "events/event-snake-forage-1.png"
   "events/event-snake-forage-2.png"
   "events/event-snake-forage-3.png"
   "events/event-snake-forage-4.png"
   "events/event-drought-skull.png"
   "events/beetle-left/beetle-1.png"
   "events/beetle-left/beetle-2.png"
   "events/beetle-left/beetle-3.png"
   "events/beetle-left/beetle-4.png"
   "events/beetle-right/beetle-1.png"
   "events/beetle-right/beetle-2.png"
   "events/beetle-right/beetle-3.png"
   "events/beetle-right/beetle-4.png"
   "sprites/glow.png"
   "sprites/pup-glow.png"
   "clock.png"
   "pointer.png"
   "prey/snail.png"
   "prey/egg.png"
   "prey/beetle.png"
   "prey/rubbish.png"
   "prey/lizard.png"
   "prey/fruit.png"
   "prey/caterpillar.png"
   "prey/tick.png"
   "pages/about.png"
   "pages/about2.png"
   ))
 (lambda ()
   (start-game canvas ctx)))

