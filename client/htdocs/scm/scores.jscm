;; -*- mode: scheme; -*-
;; Why We Love Wasps Copyright (C) 2019 FoAM Kernow
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define (pack-scores births deaths max-pop generations foraged grown-ups final-lifeforce pup-deaths young-deaths)
  (list births deaths max-pop generations foraged grown-ups final-lifeforce pup-deaths young-deaths))

;; add free starting cell
(define current-scores (pack-scores 0 0 0 0 0 0 0 0 0))

(define (reset-scores!)
  (set! current-scores (pack-scores 0 0 0 0 0 0 0 0 0)))

(define (update-scores i v)
  (set! current-scores
	(list-replace
	 current-scores i (+ (list-ref current-scores i) v))))

(define (update-scores-births! v) (update-scores 0 v))	
(define (update-scores-deaths! v) (update-scores 1 v))	
(define (update-scores-max-pop! v)
  (when (> v (scores-max-pop))
		(set! current-scores (list-replace current-scores 2 v))))
(define (update-scores-generations! v)	
  (when (> v (scores-generations))
		(set! current-scores (list-replace current-scores 3 v))))
(define (update-scores-foraged! v) (update-scores 4 v))
(define (update-scores-grown-ups! v) (update-scores 5 v))
(define (update-scores-final-lifeforce! v) (update-scores 6 v))
(define (update-scores-pup-deaths! v) (update-scores 7 v))
(define (update-scores-young-deaths! v) (update-scores 8 v))

(define (scores-births) (list-ref current-scores 0))	
(define (scores-deaths) (list-ref current-scores 1))	
(define (scores-max-pop) (list-ref current-scores 2))	
(define (scores-generations) (list-ref current-scores 3))	
(define (scores-foraged) (list-ref current-scores 4))
(define (scores-grown-ups) (list-ref current-scores 5))
(define (scores-final-lifeforce) (list-ref current-scores 6))
(define (scores-pup-deaths) (list-ref current-scores 7))
(define (scores-young-deaths) (list-ref current-scores 8))

(define current-analysis "")

(define (clear-scores-analysis!)
  (set! current-analysis ""))

(define (scores-analysis)
  (when (eq? current-analysis "")
	  (set! current-analysis (calc-scores-analysis)))
  current-analysis)

(define (add-if-null v l)
  (if (null? l) (list v) l))

(define (calc-scores-analysis)
  (string-append
   (if (> (scores-final-lifeforce) 0)
	   "Well done. "
	   "Bad luck. ")
   (choose
	(add-if-null
	 "Can you do better next time?"	 
	 (append
	  (if (zero? (scores-births))
		  (list (trans 'analysis-no-pups)) '())
	  (if (> (scores-pup-deaths) 0)
		  (list (trans 'analysis-babysit)) '())
	  (if (> (scores-young-deaths) 0)
		  (list (trans 'analysis-escort)) '())
	  (if (< (scores-generations) 2)
		  (list (trans 'analysis-new-generations)) '())
	  (if (< (scores-foraged) 1)
		  (list (trans 'analysis-no-foraging)) '())
	  (if (> (scores-foraged) 100)
		  (list (trans 'analysis-lots-foraging)) '())
	  (if (and (< (scores-max-pop) 6))
		  (list (trans 'analysis-small-pack)) '())
	  (if (and (> (scores-max-pop) 6)
			   (< (scores-max-pop) 12))
		  (list (trans 'analysis-medium-pack)) '())
	  (if (> (scores-max-pop) 12)
		  (list (trans 'analysis-large-pack)) '())
	  (if (zero? (scores-deaths))
		  (list (trans 'analysis-no-deaths)) '())
	  (if (> (scores-deaths) 5)
		  (list (trans 'analysis-no-deaths)) '())
	  (if (and
		   (> (scores-births) 0)
		   (zero? (scores-pup-deaths)))
		  (list (trans 'analysis-no-pup-deaths)) '())
	  (if (and
		   (> (scores-births) 0)
		   (zero? (scores-young-deaths)))
		  (list (trans 'analysis-no-young-deaths)) '()))))))

