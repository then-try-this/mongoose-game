;; -*- mode: scheme; -*-
;; Why We Love Wasps Copyright (C) 2019 FoAM Kernow
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define info-duration 7)
(define info-short-duration 6)

(define (make-info text id time show-time)
  (list text id time show-time))

(define (info-text i) (list-ref i 0))
(define (info-id i) (list-ref i 1))
(define (info-time i) (list-ref i 2))
(define (info-modify-time i v) (list-replace i 2 v))
(define (info-show-time i) (list-ref i 3))
(define (info-modify-show-time i v) (list-replace i 3 v))

(define (make-info-display items seen)
  (list items seen))

(define (info-display-items d) (list-ref d 0))
(define (info-display-seen d) (list-ref d 1))

(define (info-display-seen? d id)
  (cond
   ((eq? id 'allow-dup) #f)
   ((eq? id 'allow-dups) #f)
   ((eq? id #f) #f)
   ((eq? id undefined) #f)
   (else (list-contains? (info-display-seen d) id))))

(define (info-display-contains? d text)
  (foldl
   (lambda (i r)
	 (if (and (not r) (eq? text (info-text i)))
		 #t r))
   #f
   (info-display-items d)))

(define (info-display-add d i)
  (if (or	   
	   (info-display-seen? d (info-id i))
	   (info-display-contains? d (info-text i)))
      d
	  (begin
		(play-sound "notify.wav")
		(make-info-display
		 (append (info-display-items d) (list i))
		 (cons (info-id i) (info-display-seen d))))))

(define (info-display-inhibit d id)
  (if (not (info-display-seen? d id))
	  (make-info-display
	   (info-display-items d)
	   (cons id (info-display-seen d)))
	  d))

(define (info-display-update d delta)
  (make-info-display
   (filter
    (lambda (i)
      (< (info-time i)
		 (if (> (length (info-display-items d)) 1)
			 info-short-duration
			 info-duration)))
	(map
	 (lambda (i)
	   (info-modify-show-time i (+ (info-show-time i) delta)))
	 (if (not (null? (info-display-items d)))
		 (cons
		  (info-modify-time
		   (car (info-display-items d))
		   (+ (info-time (car (info-display-items d))) delta))
		  (cdr (info-display-items d)))
		 '())))
	(info-display-seen d)))

(define info-box-x 720)
(define info-box-y 5)
(define info-box-w 500)
(define info-box-h 35)
(define info-box-m (+ info-box-x (/ info-box-w 2)))
(define info-box-gap 5)

(define (count-lines ctx text max-width)
  (car (foldl
		(lambda (word r)
		  (let ((count (car r))
				(line (cadr r)))
			(let ((test-line (+ line " " word)))
			  (let ((metrics (ctx.measureText test-line)))
				(if ((> metrics.width max-width))
					(list (+ count 1) (string-append word " "))
					(list count (string-append line " " word)))))))
		(list 1 "")
		(text.split " "))))

(define (info-box-render-box ctx xoffs yoffs time line-count)
  (let ((t (max 1 (* (- 1 (/ time info-duration 0.2)) 2
					 (+ 1 (sin (* 100 time)))))))
	(let ((r 0xf9) (g 0xbe) (b 0xaf))
	  (set! ctx.fillStyle (+ "rgb(" (* r t) "," (* g t) "," (* b t) ")"))
	  (ctx.fillRect (+ xoffs info-box-x) (+ yoffs info-box-y) info-box-w (* info-box-h line-count)))))

(define (info-box-render-text ctx text xoffs yoffs) 
  (set! ctx.fillStyle text-col)
  (wrap-text ctx text (+ xoffs 170) (+ info-box-y yoffs 26) info-box-w 30))

(define (info-display-render d ctx location)
  (let ((xoff (if (eq? location "village") 100 0))
		(ypos 0))
	(set! ctx.font "bold 18pt arvo")
	(index-for-each
	 (lambda (i item)
	   (let ((line-count (count-lines ctx (info-text item) info-box-w)))
		 (info-box-render-box ctx xoff ypos (info-show-time item) line-count)
		 (info-box-render-text ctx (info-text item) xoff ypos)
		 (set! ypos (+ ypos (* info-box-h line-count) info-box-gap))))
	 (info-display-items d))))
  

