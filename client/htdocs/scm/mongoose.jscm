;; -*- mode: scheme; -*-
;; Copyright (C) 2021 Then Try This
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define male-capabilities (list 'forage 'babysit 'escort))
(define female-capabilities (list 'forage 'babysit 'escort 'new-pup-1 'new-pup-2 'new-pup-3))
(define mongoose-pup-escort-freq 0.5)

(define mongoose-debug (local-get-number "mongoose-debug"))
(define mongoose-speed (local-get-number "mongoose-speed"))
(define mongoose-pup-speed (local-get-number "mongoose-pup-speed"))
(define mongoose-forage-speed (local-get-number "mongoose-forage-speed"))
(define mongoose-unescorted-forage-speed (local-get-number "mongoose-unescorted-forage-speed"))
(define mongoose-energy-drain (local-get-number "mongoose-energy-drain"))
(define mongoose-hungry-threshold (local-get-number "mongoose-hungry-threshold"))
(define mongoose-max-energy (local-get-number "mongoose-max-energy"))
(define mongoose-giving-birth-time (local-get-number "mongoose-laying-time"))
(define mongoose-tending-time (local-get-number "mongoose-tending-time"))
(define mongoose-forage-time (local-get-number "mongoose-forage-time"))
(define mongoose-dying-time (local-get-number "mongoose-dying-time"))
(define mongoose-reproductive-threshold (local-get-number "mongoose-reproductive-threshold"))
(define mongoose-egg-benefit (local-get-number "mongoose-egg-benefit"))
(define mongoose-lizard-benefit (local-get-number "mongoose-lizard-benefit"))
(define mongoose-beetle-benefit (local-get-number "mongoose-beetle-benefit"))
(define mongoose-caterpillar-benefit (local-get-number "mongoose-caterpillar-benefit"))
(define mongoose-snail-benefit (local-get-number "mongoose-snail-benefit"))
(define mongoose-fruit-benefit (local-get-number "mongoose-fruit-benefit"))
(define mongoose-dung-beetle-benefit (local-get-number "mongoose-dung-beetle-benefit"))
(define mongoose-rubbish-benefit (local-get-number "mongoose-rubbish-benefit"))
(define mongoose-tick-benefit (local-get-number "mongoose-tick-benefit"))
(define mongoose-birthing-time (local-get-number "mongoose-birth-time"))
(define mongoose-leave-den-time (local-get-number "mongoose-leave-den-time"))
(define mongoose-adult-time (local-get-number "mongoose-adult-time"))
(define mongoose-birth-wait-time (local-get-number "mongoose-birth-wait-time"))

(define mongoose-vars (list "2" "3"))

(define mongoose-animation
  (list
   (make-anim-move "mongoose-pup" "pup" "neutral" mongoose-vars (list "1") 8 (vec2 0 0) 'loop)
   (make-anim-move "mongoose-pup" "pup" "walk" mongoose-vars (list "1" "2" "3" "4" "5" "6") 18 (vec2 0 0) 'loop)
   (make-anim-move "mongoose-pup" "pup" "lie" mongoose-vars (list "1" "2") 8 (vec2 0 0) 'oneshot)
   (make-anim-move "mongoose-pup" "pup" "lie" mongoose-vars (list "2" "1") 8 (vec2 0 0) 'oneshot)
   (make-anim-move "mongoose-pup" "pup" "eat" mongoose-vars (list "1" "2") 8 (vec2 0 0) 'loop)
   (make-anim-move "mongoose-pup" "pup" "sit" mongoose-vars (list "1") 8 (vec2 0 0) 'loop)

   (make-anim-move "mongoose-adult" "adult" "neutral" mongoose-vars (list "1") 8 (vec2 0 0) 'loop)
   (make-anim-move "mongoose-adult" "adult" "walk" mongoose-vars (list "1" "2" "3" "4" "5" "6") 18 (vec2 0 0)  'loop)
   (make-anim-move "mongoose-adult" "adult" "dig" mongoose-vars (list "1" "2") 12 (vec2 0 0) 'oneshot)
   (make-anim-move "mongoose-adult" "adult" "dig" mongoose-vars (list "3" "4" "5") 12 (vec2 0 0) 'loop)
   (make-anim-move "mongoose-adult" "adult" "dig" mongoose-vars (list "2" "1") 12 (vec2 0 0) 'oneshot)
   (make-anim-move "mongoose-adult" "adult" "eat" mongoose-vars (list "1" "2") 5 (vec2 0 0) 'loop)
   (make-anim-move "mongoose-adult" "adult" "lie" mongoose-vars (list "1" "2") 5 (vec2 0 0) 'oneshot)
   (make-anim-move "mongoose-adult" "adult" "lie" mongoose-vars (list "2" "1") 5 (vec2 0 0) 'oneshot)
   (make-anim-move "mongoose-adult" "adult" "sit" mongoose-vars (list "1" "2") 5 (vec2 0 0) 'oneshot)
   (make-anim-move "mongoose-adult" "adult" "sit" mongoose-vars (list "2" "1") 5 (vec2 0 0) 'oneshot)
   (make-anim-move "mongoose-adult" "adult" "stand" mongoose-vars (list "1" "2" "3" "4") 12 (vec2 0 0) 'oneshot)
   (make-anim-move "mongoose-adult" "adult" "stand" mongoose-vars (list "4" "3" "2" "1" "0" ) 12 (vec2 0 0) 'oneshot)
   ))

(define mongoose-animation-pup 0)
(define mongoose-animation-pup-walk 1)
(define mongoose-animation-pup-down-start 2) ;; == sit
(define mongoose-animation-pup-down-end 3)
(define mongoose-animation-pup-eat 4)
(define mongoose-animation-pup-sit 5)

(define mongoose-animation-adult 6)
(define mongoose-animation-idle 6)
(define mongoose-animation-walk 7)
(define mongoose-animation-dig-start 8)
(define mongoose-animation-dig 9)
(define mongoose-animation-dig-end 10)
(define mongoose-animation-eat 11)
(define mongoose-animation-lie-start 12)
(define mongoose-animation-lie-end 13)
(define mongoose-animation-sit-start 14)
(define mongoose-animation-sit-end 15)
(define mongoose-animation-stand-start 16)
(define mongoose-animation-stand-end 17)

(define mongoose-prey-types
  (list 'lizard 'beetle 'caterpillar 'snail))

(define (mongoose-prey-img prey)
  (cond
   ((eq? prey 'egg) (find-image "prey/egg.png"))
   ((eq? prey 'lizard) (find-image "prey/lizard.png"))
   ((eq? prey 'beetle) (find-image "prey/beetle.png"))
   ((eq? prey 'snail) (find-image "prey/snail.png"))
   ((eq? prey 'dung-beetle) (find-image "prey/beetle.png"))
   ((eq? prey 'rubbish) (find-image "prey/rubbish.png"))
   ((eq? prey 'fruit) (find-image "prey/fruit.png"))
   ((eq? prey 'caterpillar) (find-image "prey/caterpillar.png"))
   ((eq? prey 'tick) (find-image "prey/tick.png"))
   (else (find-image "prey/snail.png"))))

(define (mongoose-prey-to-energy pack prey)
  (* (cond
	  ((eq? prey 'egg) mongoose-egg-benefit)
	  ((eq? prey 'lizard) mongoose-lizard-benefit)
	  ((eq? prey 'beetle) mongoose-beetle-benefit)
	  ((eq? prey 'caterpillar) mongoose-caterpillar-benefit)
	  ((eq? prey 'snail) mongoose-snail-benefit)
	  ((eq? prey 'dung-beetle) mongoose-dung-beetle-benefit)
	  ((eq? prey 'rubbish) mongoose-rubbish-benefit)
	  ((eq? prey 'tick) mongoose-tick-benefit)
	  (else mongoose-fruit-benefit))
	 (cond
	  ((eq? (pack-weather pack) 'rain) 2)
	  (else 1))))

(define mongoose-adult-sprite-centre (vec2 182 230))
(define mongoose-pup-sprite-centre (vec2 100 65))
(define mongoose-adult-sprite-size (vec2 365 280))
(define mongoose-pup-sprite-size (vec2 200 100))

;; the only time we make mongooses (without being eggs) is the initial queen(s)
(define (make-mongoose type energy capabilities) 
  (let ((pos (pack-pos #f pack-outside-rect)))
    (append 
     (make-entity 
      (generate-entity-id!)
      "mongoose" 
      pos 0
      mongoose-adult-sprite-size
      (choose (list "left" "right"))
      (rndf)
      (anim-load-frames
	   (list-ref mongoose-vars (if (eq? type "female") 0 1))
	   mongoose-animation)
      0 mongoose-animation-idle
	  #f 0 pos #f 0 0 (vec2 0 0)
	  mongoose-postrender
	  mongoose-adult-sprite-centre 1.0)
     (list capabilities 'state-adult #f type #f 'first energy 0 #f 0 0 energy))))

(define (make-pup pos energy parent birth-time) 
  (append 
   (make-entity 
    (generate-entity-id!)
    "mongoose" 
    pos 0
    mongoose-pup-sprite-size
	(choose (list "left" "right"))
    (rndf)
    (anim-load-frames (choose mongoose-vars) mongoose-animation)
    0 mongoose-animation-pup #f 0 pos pos 0 0 (vec2 0 0)
	mongoose-postrender
	mongoose-pup-sprite-centre 1.0)
   (list '() 'state-born #f "pup" #f (entity-id parent)
		 0.01 birth-time #f 0 (+ (mongoose-generation parent) 1) energy)))

(define (mongoose-capabilities w) (list-ref w (+ entity-size 0)))
(define (mongoose-modify-capabilities w v) (list-replace w (+ entity-size 0) v))
(define (mongoose-state w) (list-ref w (+ entity-size 1)))
(define (mongoose-modify-state w s) (list-replace w (+ entity-size 1) s))
(define (mongoose-tending-id w) (list-ref w (+ entity-size 2)))
(define (mongoose-modify-tending-id w s) (list-replace w (+ entity-size 2) s))
(define (mongoose-type w) (list-ref w (+ entity-size 3)))
(define (mongoose-modify-type w s) (list-replace w (+ entity-size 3) s))
(define (mongoose-carrying w) (list-ref w (+ entity-size 4)))
(define (mongoose-modify-carrying w s) (list-replace w (+ entity-size 4) s))
(define (mongoose-parent w) (list-ref w (+ entity-size 5)))
(define (mongoose-energy w) (list-ref w (+ entity-size 6)))
(define (mongoose-modify-energy w s) (list-replace w (+ entity-size 6) s))
(define (mongoose-birth-time w) (list-ref w (+ entity-size 7)))
;; on a youngster, this is the adult and vice versa
(define (mongoose-escort w) (list-ref w (+ entity-size 8)))
(define (mongoose-modify-escort w s)
  ;; hook in here to turn on or off glow depending on escort status
  (entity-modify-bg-image
   (list-replace w (+ entity-size 8) s)
   (if s (find-image
		  (if (eq? (mongoose-type w) 'young)
			  "sprites/pup-glow.png"
			  "sprites/glow.png")) #f)))
(define (mongoose-birthcount w) (list-ref w (+ entity-size 9)))
(define (mongoose-modify-birthcount w s) (list-replace w (+ entity-size 9) s))
(define (mongoose-generation w) (list-ref w (+ entity-size 10)))
(define (mongoose-target-energy w) (list-ref w (+ entity-size 11)))
(define (mongoose-modify-target-energy w s)
  (list-replace w (+ entity-size 11) (min mongoose-max-energy s)))

;; capabilities -> menu options
(define (mongoose-build-menu-options m p)
  (let ((cycle (time->cycle (pack-time-of-year p))))
	(foldl
	 (lambda (c r)
	   (cond
		((and (eq? c 'forage)
			  (not (pack-mongooses-foraging? p)))
		 (cons (build-menu-option c (trans 'menu-forage)) r))
		((and (eq? c 'new-pup-1) 
			  (or (eq? cycle cycle-pups) (eq? mongoose-debug 1)))
		 (cons (build-menu-option c (trans 'menu-1-pup)) r))
		 ((and (eq? c 'new-pup-2)
		 	  (eq? cycle cycle-pups))
		  (cons (build-menu-option c (trans 'menu-2-pup)) r))
		 ((and (eq? c 'new-pup-3) 
		 	  (eq? cycle cycle-pups))
		  (cons (build-menu-option c (trans 'menu-3-pup)) r))
		((and (eq? c 'babysit)
			  (pack-find-mongoose-of-type p 'pup)) 
		 (cons (build-menu-option c (trans 'menu-babysit)) r))
		((and (eq? c 'escort)
			  (not (mongoose-escort m))
			  (pack-find-unescorted-young p))
		 (cons (build-menu-option c (trans 'menu-escort)) r))
		(else r)))
	 '()
	 (mongoose-capabilities m))))

;; set up a mongoose to walk to a destination
(define (mongoose-walk-to-pos w pos)
  (entity-modify-clip 
   (entity-walk-to-pos w pos)
   (mongoose-walk-animation w)))

(define (mongoose-leave-to-forage w)
  (mongoose-modify-state 
   (mongoose-walk-to-pos w offscreen-pos)
   'state-forage-signal))

;; deal with menu clicks from the player
(define (mongoose-update-action w menu-code pack)
  ;; already checked can do action in pack-update-mouse
  (mongoose-modify-target-energy
   (cond
	((eq? menu-code 'forage) 
	 (play-sound "action-1.wav")
	 (mongoose-leave-to-forage w))

	((eq? menu-code 'new-pup-1) 
	 (play-sound "action-2.wav")
	 (let ((dest (pack-pos w pack-entrance-rect)))
	   (mongoose-modify-birthcount
		(mongoose-modify-state
		 (mongoose-walk-to-pos w dest) 
		 'state-litter-activated) 1)))

	((eq? menu-code 'new-pup-2) 
	 (play-sound "action-2.wav")
	 (let ((dest (pack-pos w pack-entrance-rect)))
	   (mongoose-modify-birthcount
		(mongoose-modify-state
		 (mongoose-walk-to-pos w dest) 
		 'state-litter-activated) 2)))
	
	((eq? menu-code 'new-pup-3) 
	 (play-sound "action-2.wav")
	 (let ((dest (pack-pos w pack-entrance-rect)))
	   (mongoose-modify-birthcount
		(mongoose-modify-state
		 (mongoose-walk-to-pos w dest) 
		 'state-litter-activated) 3)))

	((eq? menu-code 'babysit) 
	 (play-sound "action-1.wav")
	 (let ((dest (pack-pos w pack-entrance-rect)))
	   (mongoose-modify-state
		(mongoose-walk-to-pos w dest)
		'state-babysit-activated)))
	
	((eq? menu-code 'escort)
	 (play-sound "action-1.wav")
	 ;; todo: manual selection
	 (let ((target (pack-find-unescorted-young pack)))
	   ;; it's possible we coundn't find any
	   (if target
		   (mongoose-modify-state
			(mongoose-walk-to-pos
			 (mongoose-modify-escort w (entity-id target)) 
			 (entity-pos target))
			;; use this state for scanning later
			'state-escort-activated)
		   w)))
	(else w))
   (+ (mongoose-energy w) (mongoose-energy-for-action menu-code))))

(define mongoose-forage-cost (local-get-number "mongoose-forage-cost"))
(define mongoose-new-pup-cost (local-get-number "mongoose-new-pup-cost"))
(define mongoose-babysit-cost (local-get-number "mongoose-babysit-cost"))
(define mongoose-escort-cost (local-get-number "mongoose-escort-cost"))

(define (mongoose-energy-for-action action)
  (cond
   ((eq? action 'forage) mongoose-forage-cost)
   ((eq? action 'new-pup-1) mongoose-new-pup-cost)
   ((eq? action 'new-pup-2) (* mongoose-new-pup-cost 2))
   ((eq? action 'new-pup-3) (* mongoose-new-pup-cost 3))
   ((eq? action 'babysit) mongoose-babysit-cost)
   ((eq? action 'escort) mongoose-escort-cost)
   (else 0)))

(define (mongoose-can-do-action m action)
  (and
   m
   ;;(not (mongoose-giving-birth? m))
   (>= (mongoose-energy m) (- (mongoose-energy-for-action action)))))

(define (mongoose-start-wandering w)
  (mongoose-modify-state
   (mongoose-walk-to-pos
	w (if (eq? (mongoose-type w) 'pup)
		  (pack-pos w pack-den-rect)
		  (pack-pos w pack-outside-rect)))
   'state-wander))

;; failsafe check on pup escortees in case we missed the message
;; assumes we are an adult, with an escort
(define (mongoose-check-adult-escort m pack)
  (let ((pup (entity-list-slow-search (pack-mongooses pack) (mongoose-escort m))))
	;; if the pup exists and the escort matches us
	(if (and pup (eq? (mongoose-escort pup) (entity-id m)))
		m (mongoose-modify-escort m #f))))

(define (mongoose-do-idle w freq)
  (if (> (entity-timer w) freq)
	  (if (< (rndf) 0.75)
		  (entity-modify-timer
		   (if (< (rndf) 0.5)
			   (mongoose-start-wandering w)
			   (mongoose-modify-state
				w
				(if (or (eq? (mongoose-type w) 'pup)
						(eq? (mongoose-type w) 'young))					
					;; pups only have sit idle behaviour
					'state-idle-sit-start
					(choose
					 (if (mongoose-escort w)
						 ;; standing with escort glow looks silly
						 (list 'state-idle-dig-start
							   'state-idle-sit-start
							   'state-idle-lie-start)	
						 (list 'state-idle-dig-start
							   'state-idle-stand-start
							   'state-idle-sit-start
							   'state-idle-lie-start)))))) 0)
		  (entity-modify-timer w 0))
	  w))

(define (mongoose-clear-young-escort w)
  (mongoose-modify-escort
   (mongoose-modify-state w 'state-young-clear-escort)
   #f))

(define (mongoose-grow-up w)
  ;; at six months, grow up to adult
  (let ((gender (choose (list 'female 'male))))
	(mongoose-modify-capabilities
	 (entity-modify-sprite-size
	  (entity-modify-centre
	   (mongoose-modify-state
		(mongoose-modify-type
		 (entity-modify-sprite-list
		  w
		  (anim-load-frames
		   (list-ref mongoose-vars (if (eq? gender 'female) 0 1))
		   mongoose-animation))
		  gender)
		;; announce we have stopped being escorted if we are
		(if (mongoose-escort w)
			'state-young-clear-escort
			'state-grow-up))
	   mongoose-adult-sprite-centre)
	  mongoose-adult-sprite-size)
	 (if (eq? gender 'female)
		 female-capabilities
		 male-capabilities))))

(define (mongoose-babysitting? w)
  (string-starts-with? (mongoose-state w) "state_babysit"))

(define (mongoose-foraging? w)
  (string-starts-with? (mongoose-state w) "state_forage"))

(define (mongoose-in-foraging-window? w)
  (and (string-starts-with? (mongoose-state w) "state_forage")
	   (not (eq? (mongoose-state w) 'state-forage-leave))))

(define (mongoose-in-foraging-window-pos? w)
  (> (vy (entity-pos w)) 533))

(define (mongoose-giving-birth? w)
  (string-starts-with? (mongoose-state w) "state_litter"))

(define (mongoose-can-forage? m)
  (and
   (not
	(or (mongoose-foraging? m)
		(eq? (mongoose-type m) 'pup)
		(mongoose-babysitting? m)
		(mongoose-giving-birth? m)))
   (mongoose-is-alive? m)))

(define (mongoose-is-unescorted-pup? m)
  (and
   (eq? (mongoose-type m) 'young)
   (not (mongoose-escort m))
   (mongoose-is-alive? m)
   (not (mongoose-in-foraging-window? m))))

(define (mongoose-can-still-escort-pup? m)
  (and
   m
   (mongoose-is-alive? m)
   (not (mongoose-giving-birth? m))
   (not (mongoose-babysitting? m))))

(define (mongoose-get-forage-speed m)
  (cond
   ((mongoose-is-unescorted-pup? m)
	mongoose-unescorted-forage-speed)
   (else
	mongoose-forage-speed)))
   
(define (mongoose-is-alive? m)
  (not
   (or (eq? (mongoose-state m) 'state-start-dying)
	   (eq? (mongoose-state m) 'state-dying)
	   (eq? (mongoose-state m) 'state-start-eaten)
	   (eq? (mongoose-state m) 'state-eaten)
	   (eq? (mongoose-state m) 'state-died))))

(define (mongoose-sound type prob)
  (when (or (eq? undefined prob)
			(< (rndf) prob))
		(cond
		 ((eq? type 'squeak)
		  (play-sound (choose (list "squeak-1.wav" "squeak-2.wav" "squeak-3.wav"))))
		 ((eq? type 'screech)
		  (play-sound (choose (list "screech-1.wav" "screech-2.wav" "screech-3.wav" "screech-4.wav"))))
		 (else
		  (play-sound (choose (list "chatter-1.wav" "chatter-2.wav" "chatter-3.wav" "chatter-4.wav" "chatter-5.wav" "chatter-6.wav")))))))
  
		 

;; main state update function, full lifecycle of the mongoose
(define (mongoose-update w delta pack)
  (mongoose-sound 'chatter 0.001)
  
  (mongoose-update-energy
   (mongoose-update-animation
	(let ((state (mongoose-state w)))
	  (cond
	  
	   ((and (mongoose-is-alive? w)
			 (< (mongoose-energy w) 0))
		(mongoose-modify-state w 'state-start-dying))
	   
	   ;; pup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	   ((eq? state 'state-born)
		(mongoose-modify-state w 'state-pup))
	   
	   ((eq? state 'state-pup)
		(if (>= (time->month (- (pack-time-of-year pack)
								(mongoose-birth-time w)))
				mongoose-leave-den-time)
			;; at one month, leave the den
			(mongoose-walk-to-pos
			 (mongoose-modify-state w 'state-leave-den)
			 (pack-pos w pack-entrance-rect))		   
			(mongoose-do-idle w 1)))
	   
	   ((eq? state 'state-leave-den)
		(if (>= (entity-walk-time w) 1)
			(mongoose-modify-state
			 (mongoose-modify-type w 'young)
			 'state-young)
			(entity-walk w mongoose-pup-speed delta)))
	   
	   ;; young ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	   ((eq? state 'state-young)
		(if (>= (time->month (- (pack-time-of-year pack)
								(mongoose-birth-time w)))
				mongoose-adult-time)
			(mongoose-grow-up w)
			(mongoose-do-idle w 1)))
	   
	   ;; check on adult
	   ((eq? state 'state-follow-escort-start)
		(if (>= (time->month (- (pack-time-of-year pack)
								(mongoose-birth-time w)))
				mongoose-adult-time)
			(mongoose-grow-up w)
			;; get our escort
			(if (> (entity-timer w) mongoose-pup-escort-freq)
				(entity-modify-timer
				 (let ((adult (entity-list-slow-search
							   (pack-mongooses pack)
							   (mongoose-escort w))))
				   ;; maybe adult has died, giving birth, doesn't exist?
				   (if (mongoose-can-still-escort-pup? adult)
					   ;; only move if we are not close already
					   (if (> (v2dist (entity-pos adult)
									  (entity-pos w)) 100)
						   (mongoose-modify-state
							(mongoose-walk-to-pos
							 w (v2add (entity-pos adult) (vec2 0 15)))
							'state-follow-escort)
						   w)
					   ;; announce we have stopped escorting
					   (mongoose-clear-young-escort w)))
				 0)
				w)))
	   
	   ((eq? state 'state-follow-escort)
		(cond
		 ((or (>= (entity-walk-time w) 1) (> (entity-timer w) mongoose-pup-escort-freq))
		  ;; regularly recheck adult position
		  (mongoose-modify-state w 'state-follow-escort-start))
		 (else
		  (entity-walk w mongoose-pup-speed delta))))

	   ((eq? state 'state-young-clear-escort)
		(mongoose-modify-state w 'state-young-clear-escort2))
	   ((eq? state 'state-young-clear-escort2)
		(mongoose-modify-state w 'state-young-clear-escort3))
	   ((eq? state 'state-young-clear-escort3)
		;; need to leave clearing escort till here so it can
		;; be read by the escorter 
		(mongoose-modify-escort
		 (mongoose-modify-state
		  ;; this can be called when we grow into an adult
		  ;; so go to the right state here
		  w (if (not (eq? (mongoose-type w) 'young))
				'state-grow-up
				(if (mongoose-in-foraging-window-pos? w)
					;; make sure we exit foraging properly if required
					;; (escort killed while foraging)
					'state-forage-fail
					'state-young)))
		 #f))
	   
	   ((eq? state 'state-grow-up)
		(if (mongoose-in-foraging-window-pos? w)
			;; make sure we exit foraging properly if required
			(mongoose-modify-state w 'state-forage-fail)
			(mongoose-modify-state w 'state-adult)))
	   
	   ;; adult ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	   ((eq? state 'state-adult)
		(mongoose-do-idle w 5))

	   ((eq? state 'state-check-adult-escort)
		(mongoose-modify-state
		 (if (mongoose-escort w)
			 (mongoose-check-adult-escort w pack)
			 w)
		 'state-adult))
	   
	   ((eq? state 'state-idle-dig-start)
		(if (> (entity-frame w) 2)
			(entity-modify-timer
			 (mongoose-modify-state w 'state-idle-dig) 0)
			w))
	   ((eq? state 'state-idle-dig)
		(if (> (entity-timer w) 3)
			(mongoose-modify-state w 'state-idle-dig-end)
			w))
	   ((eq? state 'state-idle-dig-end)
		(if (> (entity-frame w) 2)
			(mongoose-do-idle w 1)
			w))
	   
	   ((eq? state 'state-idle-stand-start)
		(if (> (entity-timer w) 5)
			(entity-modify-timer
			 (mongoose-modify-state w 'state-idle-stand-end) 0)
			w))
	   ((eq? state 'state-idle-stand-end)
		(if (> (entity-frame w) 5)
			(mongoose-do-idle w 1)
			w))


	   ((eq? state 'state-idle-lie-start)
		(if (> (entity-timer w) 5)
			(entity-modify-timer
			 (mongoose-modify-state w 'state-idle-lie-end) 0)
			w))
	   ((eq? state 'state-idle-lie-end)
		(if (> (entity-frame w) 2)
			(mongoose-do-idle w 1)
			w))

	   ((eq? state 'state-idle-sit-start)
		(if (> (entity-timer w)
			   (if (eq? (mongoose-type w) 'young)
				   1 5))
			(entity-modify-timer
			 (mongoose-modify-state w 'state-idle-sit-end) 0)
			w))
	   ((eq? state 'state-idle-sit-end)
		(if (> (entity-frame w) 2)
			(mongoose-do-idle w 1)
			w))

	   
	   ;; give birth ------------------------------------------
	   
	   ;; go into den via entrance
	   ((eq? state 'state-litter-activated)
		(mongoose-modify-escort
		 (mongoose-modify-state w 'state-litter-activated2)
		 #f))
	   ((eq? state 'state-litter-activated2)
		(mongoose-modify-state w 'state-litter-start))
	   ((eq? state 'state-litter-start)
		(if (>= (entity-walk-time w) 1)
			(mongoose-walk-to-pos
			 (mongoose-modify-state w 'state-litter-walking-into-den)
			 (pack-pos w pack-den-rect))
			(entity-walk w mongoose-speed delta)))
	   ((eq? state 'state-litter-walking-into-den)
		(if (>= (entity-walk-time w) 1)
			(entity-modify-timer (mongoose-modify-state w 'state-litter-giving-birth) 0)
			(entity-walk w mongoose-speed delta)))
	   ((eq? state 'state-litter-giving-birth) 
		(if (> (entity-timer w) mongoose-birthing-time)
			(mongoose-modify-state w 'state-litter-given-birth)
			w))
	   ((eq? state 'state-litter-given-birth)
		(mongoose-sound 'squeak 1)
		(entity-modify-timer (mongoose-modify-state w 'state-litter-birth-wait) 0))
	   ((eq? state 'state-litter-birth-wait) 
		(if (> (entity-timer w) mongoose-birth-wait-time)
			(mongoose-walk-to-pos
			 (mongoose-modify-state w 'state-litter-leaving-den)
			 (pack-pos w pack-entrance-rect))	  
			w))
	   ((eq? state 'state-litter-leaving-den)
		(if (>= (entity-walk-time w) 1)
			(mongoose-start-wandering w)
			(entity-walk w mongoose-speed delta)))
	   
	   ;; forage ------------------------------------------

	   ((eq? state 'state-forage-signal)
		(mongoose-modify-state w 'state-forage-signal2))

	   ((eq? state 'state-forage-signal2)
		(mongoose-modify-state w 'state-forage-leave))

	   ((eq? state 'state-forage-leave)
		(if (>= (entity-walk-time w) 1)
			(if (and (eq? (mongoose-type w) 'young)
					 (mongoose-escort w))
				(entity-teleport
				 ;; teleport to forage start and return
				 ;; to following the escort
				 (mongoose-modify-state w 'state-follow-escort-start)
				 (pack-pos w pack-foraging-start-rect))
				(entity-modify-timer
				 (mongoose-modify-state w 'state-forage-start) 0))
			(entity-walk w mongoose-speed delta)))

	   ((eq? state 'state-forage-start)
		  (mongoose-modify-state
		   (mongoose-walk-to-pos
			(entity-teleport w (pack-pos w pack-foraging-start-rect))
			(pack-pos w pack-foraging-end-rect))
		   'state-forage))

	   ((eq? state 'state-forage)
		(if (>= (entity-walk-time w) 1)
			;; if there is no food (attacked or drought)
			(if (and (pack-forage-event pack)
					 (or 
					  (eq? (event-type (pack-forage-event pack)) 'snake-forage)
					  (eq? (event-type (pack-forage-event pack)) 'drought)))
				;; run away...
				(mongoose-modify-state
				 (mongoose-walk-to-pos
				  w (pack-pos w pack-foraging-start-rect))
				 'state-forage-attacked)
				;; normally				
				(mongoose-modify-carrying
				 (entity-modify-timer
				  (mongoose-modify-state w 'state-forage-pause) 0)
				 (if (pack-forage-event pack)
					 ;; there should be an event, but just in case
					 (event-prey (pack-forage-event pack))
					 (choose mongoose-prey-types))))
			(entity-walk w (mongoose-get-forage-speed w) delta)))
	   
	   ((eq? state 'state-forage-pause)
		(mongoose-sound 'chatter 0.02)
		(if (> (entity-timer w) mongoose-forage-time)
			(if (and (eq? (mongoose-type w) 'young)
					 (not (mongoose-escort w)))
				;; unescorted youngsters can't forage
				(mongoose-modify-state w 'state-forage-fail)				
				(mongoose-modify-state w 'state-forage-catch))
			w))
	   
	   ((eq? state 'state-forage-catch)
		;; todo clear carrying unless feeding pups?
		(mongoose-modify-target-energy
		 (mongoose-modify-state
		  (mongoose-walk-to-pos
		   w (pack-pos w pack-foraging-start-rect))
		  'state-forage-return)
		 ;; if escorting, energy is now the same
		 (+ (mongoose-energy w)		   
			(mongoose-prey-to-energy pack (mongoose-carrying w)))))

	   ((eq? state 'state-forage-fail)
		(mongoose-modify-state
		 (mongoose-walk-to-pos
		  w (pack-pos w pack-foraging-start-rect))
		 'state-forage-return))

	   ((eq? state 'state-forage-attacked)
		(mongoose-sound 'screech 1)
		(mongoose-modify-state w 'state-forage-return))
	   
	   ((eq? state 'state-forage-return)
		(if (>= (entity-walk-time w) 1)
			(entity-modify-timer (mongoose-modify-state w 'state-forage-end) 0)
			(entity-walk w (mongoose-get-forage-speed w) delta)))

	   ((eq? state 'state-forage-end)       
		(mongoose-modify-carrying
		 (mongoose-start-wandering 
		  (entity-teleport w offscreen-pos)) #f))

	   ;; babysit ------------------------------------------

	   ((eq? state 'state-babysit-activated)
		(mongoose-modify-state
		 (mongoose-modify-escort w #f)
		 'state-babysit-activated2))
	   ((eq? state 'state-babysit-activated2)
		(mongoose-modify-state w 'state-babysit-start))
	   ((eq? state 'state-babysit-start)
		(if (>= (entity-walk-time w) 1)
			(mongoose-walk-to-pos
			 (mongoose-modify-state w 'state-babysit-walking-into-den)
			 (pack-pos w pack-den-rect))
			(entity-walk w mongoose-speed delta)))
	   ((eq? state 'state-babysit-walking-into-den)
		(if (>= (entity-walk-time w) 1)
			(entity-modify-timer (mongoose-modify-state w 'state-babysit-wait) 0)
			(entity-walk w mongoose-speed delta)))
	   ((eq? state 'state-babysit-wait) 
		;; check every 5 seconds
		(if (> (entity-timer w) 5)
			;; wait for all pups to grow up
			(if (not (pack-find-mongoose-of-type pack 'pup))				
				(mongoose-walk-to-pos
				 (mongoose-modify-state w 'state-babysit-leaving-den)
				 (pack-pos w pack-entrance-rect))
				(entity-modify-timer w 0))
			w))
	   ((eq? state 'state-babysit-leaving-den)
		(if (>= (entity-walk-time w) 1)
			(mongoose-start-wandering w)
			(entity-walk w mongoose-speed delta)))
	   
	   ;; escorting ------------------------------------------

	   ((eq? state 'state-escort-activated)
		(mongoose-modify-state w 'state-escort-activated2))
	   ;; todo: takes two ticks to register on mongoose->mongoose update
	   ((eq? state 'state-escort-activated2)
		(mongoose-modify-state w 'state-walk-to-escort))
	   ((eq? state 'state-walk-to-escort)
		(if (>= (entity-walk-time w) 1)
			(mongoose-modify-state w 'state-escort-end)
			(entity-walk w mongoose-speed delta)))
	   ((eq? state 'state-escort-end)
		(if (>= (entity-walk-time w) 1)
			(mongoose-start-wandering w)
			(entity-walk w mongoose-speed delta)))
	   
	   ;; common ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	   ((eq? state 'state-wander)
		(if (>= (entity-walk-time w) 1)		   
			(entity-modify-timer (mongoose-modify-state w 'state-end-wander) 0)
			(entity-walk
			 w (if (or (eq? (mongoose-type w) 'pup)
					   (eq? (mongoose-type w) 'young))
				   mongoose-pup-speed
				   mongoose-speed) delta)))
	   
	   ((eq? state 'state-end-wander)	   
		(if (>= (entity-timer w) 1)		   
			(entity-modify-timer
			 (mongoose-modify-state
			  w (cond
				 ((eq? (mongoose-type w) 'pup) 'state-pup)
				 ((eq? (mongoose-type w) 'young) 'state-young)
				 ;; showhorn this here
				 (else 'state-check-adult-escort)))
			 0)
			w))

	   ((eq? state 'state-start-dying)
		(mongoose-sound 'screech 1)
		(entity-modify-timer (mongoose-modify-state w 'state-dying) 0))
	   ((eq? state 'state-dying)
		(if (> (entity-timer w) mongoose-dying-time)
			(mongoose-modify-state w 'state-died)
			;; fade out when dying...
			(entity-modify-opacity w (- 1 (/ (entity-timer w) mongoose-dying-time)))))

	   ((eq? state 'state-start-eaten)
		(mongoose-sound 'screech 1)
		(entity-modify-timer (mongoose-modify-state w 'state-eaten) 0))
	   ((eq? state 'state-eaten)
		(if (> (entity-timer w) mongoose-dying-time)
			(mongoose-modify-state w 'state-died)
			;; immediately dissapear...
			(entity-modify-opacity w 0)))

	   (else w)))
	delta)
   delta))

(define (mongoose-update-energy w delta)
  (let ((drain (* mongoose-energy-drain delta)))
	 (mongoose-modify-energy
	  (mongoose-modify-target-energy
	   w (- (mongoose-target-energy w) drain))
	  (- (lerp (mongoose-energy w) (mongoose-target-energy w) (* delta 1))
		 drain))))

(define (mongoose-walk-animation w)
  (cond
   ((eq? (mongoose-carrying w) 'spider) mongoose-animation-walk)
   ((eq? (mongoose-carrying w) 'caterpillar) mongoose-animation-walk)
   ((eq? (mongoose-carrying w) 'fly) mongoose-animation-walk)
   ((or (eq? (mongoose-type w) 'pup)
		(eq? (mongoose-type w) 'young))
	mongoose-animation-pup-walk)
   (else mongoose-animation-walk)))

;; map the states to the right animation clips
(define (mongoose-update-animation w delta)  
  (let ((state (mongoose-state w)))	
	(entity-modify-clip 
     (entity-modify-timer w (+ (entity-timer w) delta))
     (cond
      ((eq? state 'state-end-wander) 
	   (if (or (eq? (mongoose-type w) 'pup)
			   (eq? (mongoose-type w) 'young))
		   mongoose-animation-pup-down-start		   
		   mongoose-animation-lie-start))
	  ((eq? state 'state-grow-up) mongoose-animation-adult)
	  ((eq? state 'state-young) mongoose-animation-pup)
	  ((eq? state 'state-young-clear-escort) mongoose-animation-pup)
	  ((eq? state 'state-young-clear-escort2) mongoose-animation-pup)
	  ((eq? state 'state-young-clear-escort3) mongoose-animation-pup)
      ((eq? state 'state-litter-giving-birth) mongoose-animation-lie-start)
      ((eq? state 'state-litter-birth-wait) mongoose-animation-lie-end) 
      ((eq? state 'state-babysit-wait) mongoose-animation-lie-start)
	  ((eq? state 'state-follow-escort-start) mongoose-animation-pup)
	  ((eq? state 'state-forage-pause)
	   (if (or (eq? (mongoose-type w) 'pup)
			   (eq? (mongoose-type w) 'young))
		   mongoose-animation-pup-eat		   
		   mongoose-animation-eat))
	  ((eq? state 'state-idle-dig-start) mongoose-animation-dig-start)
	  ((eq? state 'state-idle-dig) mongoose-animation-dig)
	  ((eq? state 'state-idle-dig-end) mongoose-animation-dig-end)
	  ((eq? state 'state-idle-stand-start) mongoose-animation-stand-start)
	  ((eq? state 'state-idle-stand-end) mongoose-animation-stand-end)
	  ((eq? state 'state-idle-sit-start)
	   (if (or (eq? (mongoose-type w) 'pup)
			   (eq? (mongoose-type w) 'young))
		   mongoose-animation-pup-sit
		   mongoose-animation-sit-start))
	  ((eq? state 'state-idle-sit-end)
	   (if (or (eq? (mongoose-type w) 'pup)
			   (eq? (mongoose-type w) 'young))
		   mongoose-animation-pup-sit
		   mongoose-animation-sit-end))
	  ((eq? state 'state-idle-lie-start) mongoose-animation-lie-start)
	  ((eq? state 'state-idle-lie-end) mongoose-animation-lie-end)
      (else (entity-clip w))))))

;; update new mongooses mainly - could be much simpler
(define (mongoose-update-mongooses pack mongoose mongooses time)
  (cond

   ;; new pups 
   ((eq? (mongoose-state mongoose) 'state-litter-given-birth)
    (append
	 (build-list
	  (mongoose-birthcount mongoose)
	  (lambda (_)
		(make-pup
		 (entity-walk-to mongoose)
		 ;; todo: divide by num pups?
		 (/ (mongoose-energy mongoose)
			(mongoose-birthcount mongoose))
		 mongoose
		 time)))
	 mongooses))

   ;; all go foraging together!!
   ((eq? (mongoose-state mongoose) 'state-forage-signal2) 
	(map
	 (lambda (other)
	   (if (mongoose-can-forage? other)
		   (mongoose-leave-to-forage other)
		   other))
	 mongooses))
   
   ;; feed the escorted pup!
   ;; (why does this work but the escort below need 2 states?)
   ((and	
	 (eq? (mongoose-state mongoose) 'state-forage-catch)
	 (mongoose-escort mongoose))
	;; only adults will trigger forage-catch, so don't need to check
	(entity-list-modify
	 mongooses (mongoose-escort mongoose)
	 (lambda (other)
	   (mongoose-modify-target-energy
		other
		(+ (mongoose-target-energy other)
		   ;; no longer divide share with escort		   
		   (mongoose-prey-to-energy pack (mongoose-carrying mongoose)))))))
   
   ;; teleport escort back up after foraging
   ((and	
	 (eq? (mongoose-state mongoose) 'state-forage-end)
	 (mongoose-escort mongoose))
	;; only adults will trigger forage-end with an escort,
	;; so don't need to check
	(entity-list-modify
	 mongooses (mongoose-escort mongoose)
	 (lambda (other)
 	   (entity-teleport
		(mongoose-modify-state other 'state-follow-escort-start)
		offscreen-pos))))
   
   ;; todo: takes two ticks to register on mongoose->mongoose update
   ((eq? (mongoose-state mongoose) 'state-escort-activated2)
	;; tell the escorted youngster that it's being escorted
    (entity-list-modify 
     mongooses (mongoose-escort mongoose)
     (lambda (other)
	   ;; unlikely this is true as we now check during action
	   (if (mongoose-is-alive? mongoose)
		   (entity-modify-timer
			(mongoose-modify-escort
			 (mongoose-modify-state other 'state-follow-escort-start)
			 (entity-id mongoose))
			0)
		   mongoose))))
   
   ;; todo: takes two ticks to register on mongoose->mongoose update
   ((eq? (mongoose-state mongoose) 'state-young-clear-escort2)
	;; tell the escorting adult that it is no longer escorting
	(entity-list-modify 
	 mongooses (mongoose-escort mongoose)
	 (lambda (other)
	   (mongoose-modify-escort other #f))))
   
   ;; check died mongooses and tell their escorts
   ((eq? (mongoose-state mongoose) 'state-died)
	(if (mongoose-escort mongoose)
		;; tell the escorting adult that it is no longer escorting
		(entity-list-modify 
		 mongooses (mongoose-escort mongoose)
		 (lambda (other)
		   (mongoose-modify-escort other #f)))
		mongooses))
   
   (else mongooses)))

(define (mongoose-render-energy-bar e ctx)
	(set! ctx.fillStyle bg-col)
	(ctx.fillRect 0 0 menu-width 27)
	(set! ctx.fillStyle highlight2-col)
	(ctx.fillRect 0 0 (* (/ (max 0 (mongoose-energy e)) mongoose-max-energy)
						 menu-width)
				  27)
	(let ((text (string-append
				 "LIFE FORCE: "
				 (trunc (* (/ (max 0 (mongoose-energy e))
							  mongoose-max-energy)
						   100))
				 "%")))
	  (set! ctx.fillStyle text-col)
	  (let ((m (ctx.measureText text)))
		(ctx.fillText
		 text
		 (- (/ menu-width 2) (/ m.width 2))
		 21))))

(define (mongoose-postrender e ctx time)

  ;; (let ((scale-x 1) (scale-y 1))
  ;; 	(let ((w (* (vx (entity-sprite-size e)) scale-x))
  ;; 		  (h (* (vy (entity-sprite-size e)) scale-y)))
  ;; 	(set! ctx.strokeStyle "#777")
  ;; 	(ctx.strokeRect
  ;; 	 (- (vx (entity-pos e)) (vx (entity-centre e)))
  ;; 	 (- (vy (entity-pos e)) (vy (entity-centre e)))
  ;; 	 w h)))
  
  (ctx.save)
  (ctx.translate (vx (entity-pos e)) (vy (entity-pos e)))
  
  (when (or (> (abs (- (mongoose-energy e) (mongoose-target-energy e)))
			   0.01)
			(and (< (mongoose-energy e) mongoose-hungry-threshold)
				 (zero? (modulo (floor (/ time 300)) 2))))
		(ctx.save)
		(ctx.translate (/ menu-width -2)
					   (if (or (eq? (mongoose-type e) 'pup)
							   (eq? (mongoose-type e) 'young))
						   -110 -140))
		(set! ctx.font "normal 16pt arvo")
		(set! ctx.fillStyle text-col)
		(mongoose-render-energy-bar e ctx)
		(ctx.restore))


	;; (when (and (mongoose-is-alive? e)
	;; 		   (< (mongoose-energy e) mongoose-hungry-threshold))
	;; 	  (set! ctx.fillStyle highlight-col)
	;; 	  (ctx.fillRect -75 -70 150 30)
	;; 	  (set! ctx.font "normal 16pt arvo")
	;; 	  (set! ctx.fillStyle text-col)
	;; 	  (fill-centre-text ctx (trans 'mongoose-hungry) 0 -50))

		  ;; (ctx.strokeRect (- (/ (vx (entity-sprite-size e)) 2))
		  ;; 				  (- (/ (vy (entity-sprite-size e)) 2))
		  ;; 				  (vx (entity-sprite-size e))
		  ;; 				  (vy (entity-sprite-size e)))

	
	(when (eq? mongoose-debug 1)
		  
		  (set! ctx.strokeStyle "#777")

		  (set! ctx.strokeStyle "#fff")
		  (set! ctx.lineWidth 3)
		  (ctx.beginPath)
		  (ctx.moveTo -10 0)
		  (ctx.lineTo 10 0)
		  (ctx.stroke)
		  (ctx.beginPath)
		  (ctx.moveTo 0 -10)
		  (ctx.lineTo 0 10)
		  (ctx.stroke)
		  (set! ctx.lineWidth 1)
		  
		  (set! ctx.font "normal 16pt open-sans")
		  (set! ctx.fillStyle "#007")
		  (let ((x -60) (y -120) (ygap 20) (width 500) (lineh 15))
			(wrap-text-left ctx (string-append "mongoose: " (entity-id e)) x y width lineh)
			(set! y (+ y ygap))
			(wrap-text-left ctx (mongoose-state e) x y width lineh)
			(set! y (+ y ygap))
			(wrap-text-left ctx (string-append "energy: " (let ((n (mongoose-energy e)))
															(n.toFixed 2))) x y width lineh)
			(set! y (+ y ygap))
			(wrap-text-left ctx (string-append "type: " (mongoose-type e)) x y width lineh)
			(set! y (+ y ygap)) 
			(wrap-text-left ctx (string-append "escorting: " (cond
															  ((and (mongoose-escort e)
																	(eq? (mongoose-type e) 'young))
															   (string-append "escorted by " (mongoose-escort e)))
															  ((and (mongoose-escort e)
																	(not (eq? (mongoose-type e) 'young)))
															   (string-append "escorting " (mongoose-escort e)))
															  (else "no"))) x y width lineh)
			(set! y (+ y ygap))
			))
	
	;;---------------------------------------------------

	
	(ctx.restore)
	)

